package mobi.app4mob.totalstopcard.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import mobi.app4mob.total.stopcard.R;
import mobi.app4mob.totalstopcard.activities.DetailsHistoryActivity;
import mobi.app4mob.totalstopcard.adapters.HistoryAdapter;
import mobi.app4mob.totalstopcard.models.StopCard;


/**
 * A simple {@link Fragment} subclass.
 */
public class AnnomalieFragment extends Fragment {

    private ArrayList<StopCard> data ;
    ListView list;
    HistoryAdapter listAdapter;
    public AnnomalieFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            data = (ArrayList<StopCard>) getArguments().getSerializable("list");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_annomalie, container, false);
        list=(ListView)view.findViewById(R.id.list) ;
        if (data==null)
            data = new ArrayList<>();
        listAdapter = new HistoryAdapter(getContext(), data,3);
        list.setAdapter(listAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                StopCard stopCard = data.get(position);
                Intent intent = new Intent(getActivity(), DetailsHistoryActivity.class);
                intent.putExtra("stopcard", stopCard);
                intent.putExtra("type", "annomalie");
                startActivity(intent);
            }
        });
        return view;
    }

}
