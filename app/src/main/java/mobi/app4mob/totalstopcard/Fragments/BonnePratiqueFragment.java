package mobi.app4mob.totalstopcard.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.ArrayList;

import mobi.app4mob.total.stopcard.R;
import mobi.app4mob.totalstopcard.activities.DetailsHistoryActivity;
import mobi.app4mob.totalstopcard.activities.HistoryStopCardActivity;
import mobi.app4mob.totalstopcard.adapters.HistoryAdapter;
import mobi.app4mob.totalstopcard.models.StopCard;
import mobi.app4mob.totalstopcard.models.User;
import mobi.app4mob.totalstopcard.utils.SessionManager;


/**
 * A simple {@link Fragment} subclass.
 */
public class BonnePratiqueFragment extends Fragment {

    private ArrayList<StopCard> data ;
    ListView list;
    HistoryAdapter listAdapter;
    public BonnePratiqueFragment(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            data = (ArrayList<StopCard>) getArguments().getSerializable("list");
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_bonne_pratique, container, false);
        list=(ListView)view.findViewById(R.id.list) ;
        if (data==null)
            data = new ArrayList<>();
        listAdapter = new HistoryAdapter(getContext(), data,2);
        list.setAdapter(listAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                StopCard stopCard = data.get(position);
                Intent intent = new Intent(getActivity(), DetailsHistoryActivity.class);
                intent.putExtra("stopcard", stopCard);
                intent.putExtra("type", "bp");
                startActivity(intent);
            }
        });
        return view;
    }


}
