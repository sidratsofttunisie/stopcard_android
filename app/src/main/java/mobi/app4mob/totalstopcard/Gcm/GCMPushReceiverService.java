package mobi.app4mob.totalstopcard.Gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import java.util.Locale;

import mobi.app4mob.total.stopcard.R;
import mobi.app4mob.totalstopcard.activities.DetailsNotificationActivity;
import mobi.app4mob.totalstopcard.activities.HistoryActivity;
import mobi.app4mob.totalstopcard.activities.HomeActivity;
import mobi.app4mob.totalstopcard.activities.LastMultiStopCardActivity;
import mobi.app4mob.totalstopcard.activities.LastStopCardActivity;
import mobi.app4mob.totalstopcard.activities.ProxyActivity;
import mobi.app4mob.totalstopcard.activities.SafetyCheckListActivity;
import mobi.app4mob.totalstopcard.models.StopCardHistory;
import mobi.app4mob.totalstopcard.utils.SessionManager;

/**
 * Created by adnen.chouibi@gmail.com on 27/02/2017.
 */

public class GCMPushReceiverService extends GcmListenerService {


    //This method will be called on every new message received
    @Override
    public void onMessageReceived(String from, Bundle data) {
        //Getting the message from the bundle
        String sender_name = data.getString("name");
        String message = "StopCard a été emis par "+sender_name;
        if(Locale.getDefault().getLanguage().equals("en"))
                 message = "StopCard was sent by "+sender_name;

        //Displaying a notiffication with the message

        if(data.getString("context").equals("safetyNotification")){

            sendNotification(data.getString("message"),true,data.getString("notif"),false);
        }else if(data.getString("context").equals("pushNotification"))
            sendNotification(data.getString("message"),false,data.getString("notif"),true);
        else
            sendNotification(message,false,null,false);
    }

    //This method is generating a notification and displaying the notification
    private void sendNotification(String message,boolean is_safety_notif,String notif,boolean is_push) {
        Intent intent ;
        if(is_safety_notif){
            intent = new Intent(this, DetailsNotificationActivity.class);
            intent.putExtra("fromExtras",false);
            intent.putExtra("notif_id",notif);
        }else if(is_push){
            intent = new Intent(this, HomeActivity.class);

        }else{
            intent = new Intent(this, LastStopCardActivity.class);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        int requestCode = 0;
        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestCode, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder noBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(message)
                .setContentTitle("StopCard")
                .setSound(sound)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, noBuilder.build()); //0 = ID of notification
    }
}