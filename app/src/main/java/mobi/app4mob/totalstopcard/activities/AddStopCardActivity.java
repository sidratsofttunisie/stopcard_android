package mobi.app4mob.totalstopcard.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.DocumentsContract;
import android.provider.DocumentsProvider;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v4.content.FileProvider;
import android.support.v4.provider.DocumentFile;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Locale;

import id.zelory.compressor.Compressor;
import mobi.app4mob.total.stopcard.BuildConfig;
import mobi.app4mob.total.stopcard.R;
import mobi.app4mob.totalstopcard.models.Lieu;
import mobi.app4mob.totalstopcard.models.PostResponse;
import mobi.app4mob.totalstopcard.models.User;
import mobi.app4mob.totalstopcard.service.ApiService;
import mobi.app4mob.totalstopcard.service.RestClient;
import mobi.app4mob.totalstopcard.utils.SessionManager;
import mobi.app4mob.totalstopcard.utils.Utils;
import nl.changer.polypicker.Config;
import nl.changer.polypicker.ImagePickerActivity;
import nl.changer.polypicker.model.Image;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class AddStopCardActivity extends BaseActivity implements LocationListener {
    ImageView incident_state;
    ImageView lieu_state;
    TextView lieu_name;
    ImageView photo_state;
    ImageView annomalie_btn, stopcard_btn, bonne_pratique_btn;
    private int type = -1;
    TextView annomalies;
    TextView stopcards;
    TextView bestpractices;

    boolean has_photo = false;

    int[] selected_incident = new int[12];
    String selected_incident_string = "";
    int selected_lieu_id = 0;

    ArrayList<Lieu> lieux = new ArrayList<Lieu>();
    ProgressDialog prgDialog;
    public static int TAKE_PIC = 1;
    public static int SELECT_PHOTO = 2;
    public static int SELECT_INCIDENT = 3;
    public static int SELECT_PLACE = 4;

    private LocationManager locationManager;
    private Location location;
    private Boolean place_is_loading = false;
    private File file_to_upload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_stop_card);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Utils.checkPermissions(this);
        initSession();
        if (!this.user.getHas_annomalies_bonnes_pratiques() && !this.user.getHas_annomalies() && !this.user.getHas_bonnes_pratiques())
            type = 1;
        initView();
        // loadLieu();
        try {
            getPostion();
        } catch (SecurityException e) {
            Utils.checkPermissions(this);
        }

    }

    public void initView() {
        TextView page_title = (TextView) findViewById(R.id.page_title);
        page_title.setText(getString(R.string.add_stopcard_title));
        if (session.getUserDetails().getHas_stop_card()
                || session.getUserDetails().getHas_bonnes_pratiques() || session.getUserDetails().getHas_annomalies()) {
            if (session.getUserDetails().getHas_annomalies()) {
                annomalie_btn = (ImageView) findViewById(R.id.annomalie_btn);
                annomalies = (TextView) findViewById(R.id.annomalies);
            }
            if (session.getUserDetails().getHas_bonnes_pratiques()) {
                bonne_pratique_btn = (ImageView) findViewById(R.id.bonne_pratique_btn);
                bestpractices = (TextView) findViewById(R.id.bestpractices);
            }
            if (session.getUserDetails().getHas_stop_card()) {
                stopcard_btn = (ImageView) findViewById(R.id.stopcard_btn);
                stopcards = (TextView) findViewById(R.id.stopcards);
            }


        }
        if (!session.getUserDetails().getHas_bonnes_pratiques()) {
            ((RelativeLayout) findViewById(R.id.bonnes_pratiques_layout)).setVisibility(View.GONE);
        }
        if (!session.getUserDetails().getHas_annomalies()) {
            ((RelativeLayout) findViewById(R.id.annomalies_layout)).setVisibility(View.GONE);
        }
        if (!session.getUserDetails().getHas_stop_card()) {
            ((RelativeLayout) findViewById(R.id.stopcard_layout)).setVisibility(View.GONE);
        }
        if (!session.getUserDetails().getHas_annomalies() && !session.getUserDetails().getHas_bonnes_pratiques() && !session.getUserDetails().getHas_stop_card()) {
            ((LinearLayout) findViewById(R.id.stopcard_type_layout)).setVisibility(View.GONE);
            type = 1;
        }

    }

    public void selectIncident(View v) {
        Intent intent = new Intent(AddStopCardActivity.this, IncidentActivity.class);
        intent.putExtra("selected_incident", selected_incident);
        Log.e("add incidents", String.valueOf(selected_incident[0]));
        startActivityForResult(intent, SELECT_INCIDENT);
    }

    public void selectLieu(View v) {
        Intent intent = new Intent(AddStopCardActivity.this, LieuActivity.class);
        intent.putExtra("lieux", lieux);
        startActivityForResult(intent, SELECT_PLACE);
    }

    public void camera(View v) {
        final CharSequence[] items = {"Camera", "Gallery", getString(R.string.cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(AddStopCardActivity.this);
        builder.setTitle(getString(R.string.add_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Camera")) {
                    if (ActivityCompat.checkSelfPermission(AddStopCardActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        Utils.checkPermissions(AddStopCardActivity.this);
                    } else {
                        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "stopcard.jpg");
                        Uri uri = FileProvider.getUriForFile(AddStopCardActivity.this,
                                BuildConfig.APPLICATION_ID + ".provider",
                                file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                        startActivityForResult(intent, TAKE_PIC);
                    }
                } else if (items[item].equals("Gallery")) {
                    if (ActivityCompat.checkSelfPermission(AddStopCardActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        Utils.checkPermissions(AddStopCardActivity.this);
                    } else {
                        Intent intent = new Intent(getBaseContext(), ImagePickerActivity.class);
                        Config config = new Config.Builder()
                                .setTabBackgroundColor(R.color.white)    // set tab background color. Default white.
                                .setTabSelectionIndicatorColor(R.color.blue)
                                .setCameraButtonColor(R.color.green)
                                .setSelectionLimit(1)    // set photo selection limit. Default unlimited selection.
                                .build();
                        ImagePickerActivity.setConfig(config);
                        startActivityForResult(intent, SELECT_PHOTO);
                    }
                } else if (items[item].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    public void takePic(Intent data) {
        has_photo = true;
        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "stopcard.jpg");
        Bitmap bitmap = decodeSampledBitmapFromFile(file.getAbsolutePath(), 1000, 700);
        try {
            file_to_upload = new Compressor(this)
                    .setMaxWidth(640)
                    .setMaxHeight(480)
                    .setQuality(50)
                    .setCompressFormat(Bitmap.CompressFormat.JPEG)
                    .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).getAbsolutePath())
                    .compressToFile(file);

        } catch (Exception e) {
            Crashlytics.logException(e);
            file_to_upload = file;
        }
        validatePhoto();
    }

    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) {
        // BEST QUALITY MATCH
        //First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float) height / (float) reqHeight);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth) {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float) width / (float) reqWidth);
        }
        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    public void selectPhoto(Intent data) {
        has_photo = true;
        if (data != null) {
            HashSet<Uri> mMedia = new HashSet<Uri>();
            Parcelable[] parcelableUris = data.getParcelableArrayExtra(ImagePickerActivity.EXTRA_IMAGE_URIS);

            if (parcelableUris == null) {
                return;
            }
            // Java doesn't allow array casting, this is a little hack
            Uri[] uris = new Uri[parcelableUris.length];
            System.arraycopy(parcelableUris, 0, uris, 0, parcelableUris.length);
            if (uris != null) {
                for (Uri uri : uris) {
                    mMedia.add(uri);
                }
            }
            Uri selectedImage = mMedia.iterator().next();
            File file = new File(selectedImage.getPath());
            try {
                file_to_upload = new Compressor(this)
                        .setMaxWidth(640)
                        .setMaxHeight(480)
                        .setQuality(50)
                        .setCompressFormat(Bitmap.CompressFormat.JPEG)
                        .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_PICTURES).getAbsolutePath())
                        .compressToFile(file);

            } catch (Exception e) {
                Crashlytics.logException(e);
                file_to_upload = file;
            }

            //photo_state.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ajout_photo, 0, R.drawable.ok, 0);
            validatePhoto();
        }
    }

    public void validatePhoto() {

        TextView add_photo_label = (TextView) findViewById(R.id.add_photo_label);
        ImageView state = (ImageView) findViewById(R.id.state);
        state.setVisibility(View.VISIBLE);
        add_photo_label.setText("Photo ajoutée");
    }

    public void selectIncidentIntent(Intent data) {
        incident_state = (ImageView) findViewById(R.id.incidentState);
        selected_incident = data.getIntArrayExtra("result");
        selected_incident_string = "";
        String categories = "";
        for (int i = 0; i < selected_incident.length; i++) {
            if (selected_incident[i] > 0) {
                categories = categories + selected_incident[i] + ",";
            }
        }
        if (categories.length() > 1) {
            selected_incident_string = categories.substring(0, categories.length() - 1);
        }
        if (!selected_incident_string.equals("")) {
            incident_state.setImageResource(R.drawable.ok);
        } else {
            incident_state.setImageResource(R.drawable.flech_acc);
        }
        Log.i(">>>", ">>>selected_incident_string" + selected_incident_string);
    }

    public void selectLieuIntent(Intent data) {
        selected_lieu_id = data.getIntExtra("result", 0);
        int position = data.getIntExtra("position", 0);
        lieu_state = (ImageView) findViewById(R.id.lieuState);
        lieu_name = (TextView) findViewById(R.id.lieux);
        if (selected_lieu_id > 0) {
            lieu_state.setImageResource(R.drawable.ok);
            lieu_name.setText(lieux.get(position).getLibelle());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(">>>>>>>>>", ">>>> REQUEST CODE <<<<<<" + requestCode);
        if (resultCode == RESULT_OK) {
            if (requestCode == TAKE_PIC) {
                takePic(data);

            } else if (requestCode == SELECT_PHOTO) {
                try {
                    selectPhoto(data);
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
            } else if (requestCode == SELECT_INCIDENT) {
                selectIncidentIntent(data);
            } else if (requestCode == SELECT_PLACE) {
                selectLieuIntent(data);
            }
        }
    }


    public void upload(View v) {
        EditText description_edit = (EditText) findViewById(R.id.description);
        EditText action_edit = (EditText) findViewById(R.id.action);
        String description_ = description_edit.getText().toString().trim();
        String action_ = action_edit.getText().toString().trim();
        boolean has_error = false;
        String message = "";
        /*if (selected_incident_string.equals("")) {
            has_error = true;
            message += "- " + getString(R.string.error_type_incident) + " \n";
        }*/
        if (selected_lieu_id == 0) {
            has_error = true;
            message += "- " + getString(R.string.error_location) + " \n";
        }
        if (type == -1) {
            has_error = true;
            message += "- " + getString(R.string.error_type) + " \n";
        }

        if (has_error) {
            showAlert(message);
        } else {


            prgDialog = new ProgressDialog(this);
            prgDialog.setMessage(getString(R.string.loading));
            prgDialog.setCancelable(false);
            prgDialog.show();


            ApiService service = RestClient.getClient();

            Call<PostResponse> call;


            RequestBody description =
                    RequestBody.create(MediaType.parse("multipart/form-data"), description_);
            RequestBody action =
                    RequestBody.create(MediaType.parse("multipart/form-data"), action_);

            RequestBody categories =
                    RequestBody.create(MediaType.parse("multipart/form-data"), selected_incident_string);

            RequestBody user_id =
                    RequestBody.create(MediaType.parse("multipart/form-data"), user.getId() + "");

            RequestBody lieu_id =
                    RequestBody.create(MediaType.parse("multipart/form-data"), selected_lieu_id + "");

            String locale = "fr";
            if (Locale.getDefault().getDisplayLanguage().equals("English"))
                locale = "en";
            else if (Locale.getDefault().getDisplayLanguage().equals("español"))
                locale = "es";

            if (has_photo) {
                Log.i(">>>>>", "><<><<<<>>>> categories" + selected_incident_string);
                // File file = new File("path/to/your/file");
                RequestBody requestBody =
                        RequestBody.create(MediaType.parse("multipart/form-data"), file_to_upload);
                RequestBody locale_ =
                        RequestBody.create(MediaType.parse("multipart/form-data"), locale);

                call = service.addPublication(requestBody, description, action, categories, type, user_id, lieu_id, locale_);
            } else {
                call = service.addPublicationWithoutImage(description_, action_, selected_incident_string, user.getId() + "", type, selected_lieu_id + "", locale);
            }

            call.enqueue(new Callback<PostResponse>() {
                @Override
                public void onResponse(Response<PostResponse> response, Retrofit retrofit) {
                    prgDialog.hide();
                    PostResponse result = response.body();
                    if (result.getMessage().equals(ApiService.SUCCESS)) {
                        Log.i(">>><", ">>><<<<SUCESSS>>>><<<");
                        if (type == 1)
                            user.setNbStopCards(user.getNbStopCards() + 1);
                        else if (type == 2)
                            user.setNbBonPratiques(user.getNbBonPratiques() + 1);
                        else
                            user.setNbAnnomalies(user.getNbAnnomalies() + 1);
                        session.updateSession(user);

                        Thread thread = new Thread() {
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(800);
                                    AddStopCardActivity.this.finish();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        };

                        Toast.makeText(getBaseContext(), getResources().getString(R.string.success_msg), Toast.LENGTH_LONG).show();
                        thread.start();
                    }
                    Log.i(">>><", "1/ " + result.getMessage().toString());
                    Log.i(">>><", "2/ " + result.getData().toString());

                }

                @Override
                public void onFailure(Throwable t) {
                    prgDialog.hide();
                    showAlert(getResources().getString(R.string.error_loading));
                    Log.i(">>><", ">>><<<<ERRORR>>>><<<" + t.getMessage().toString());
                }
            });
        }
    }

    public void loadLieu() {

        place_is_loading = true;

        ApiService service = RestClient.getClient();
        Log.i(">>>>", ">>>< country " + user.getCountry());
        Call<ArrayList<Lieu>> call;
        if (this.location != null) {
            call = service.getLieux(user.getCountry(), location.getLatitude(), location.getLongitude());
            Log.i(">>>>>>", "longitude = " + this.location.getLongitude() + " >>> latitude = " + this.location.getLatitude());
        } else {
            Log.i(">>>>>>", "no location");
            call = service.getLieux(user.getCountry(), 0, 0);
        }

        call.enqueue(new Callback<ArrayList<Lieu>>() {
            @Override
            public void onResponse(Response<ArrayList<Lieu>> response, Retrofit retrofit) {
//                prgDialog.hide();
                lieux = response.body();
                Log.i(">>>>", ">>>" + lieux.size());
                place_is_loading = false;
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(">>>>", ">>>" + t.getMessage().toString());
//                prgDialog.hide();
                showAlert(getString(R.string.error_loading));
                place_is_loading = false;
            }
        });
    }

    private void showAlert(String message) {
        try {
            new AlertDialog.Builder(this)
                    .setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.valider), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            dialog.dismiss();
                        }
                    }).show();
        } catch (Exception e) {
            Crashlytics.logException(e);
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (prgDialog != null) {
            if (prgDialog.isShowing())
                prgDialog.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (prgDialog != null) {
            if (prgDialog.isShowing())
                prgDialog.dismiss();
        }
    }

    public void getPostion() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Boolean isGPSEnabled;
        Boolean isNetworkEnabled;

        // getting GPS status
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        // getting network status
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isGPSEnabled && !isNetworkEnabled) {
            displayLocationSettingsRequest();
            //nothing
        } else {   // Either GPS provider or network provider is enabled

            // if GPS Enabled get lat/long using GPS Services
            if (isGPSEnabled) {
                // test if gps permission is granted otherwise application is going to crash in api >=23
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    Utils.checkPermissions(this);
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                } else {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 1, this);
                    if (locationManager != null) {
                        Log.i(">>>", "locationmanger != null");
                        this.location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    }
                }


            }// End of if GPS Enabled
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Utils.checkPermissions(this);
            } else {
                //  get location from Network Provider
                if (isNetworkEnabled) {
                    Log.i(">>>", "network active");
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 1, this);
                    if (locationManager != null) {
                        Log.i(">>>", "locationmanger != null");
                        this.location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    }
                }
            }// End of IF network enabled

        }// End of Either GPS provider or network provider is enabled
        loadLieu();
    }

    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
        Log.i(">>>>>>", "longitude = " + location.getLongitude() + " >>> latitude = " + location.getLatitude());
        if (!place_is_loading) loadLieu();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.i(">>>>>>", "Statut changed!! ");
    }

    @Override
    public void onProviderEnabled(String provider) {

        if (!place_is_loading) loadLieu();
        Log.i(">>>>>>", "Gps is turned on!! ");
    }

    @Override
    public void onProviderDisabled(String provider) {
        displayLocationSettingsRequest();
        /*Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
        Log.i(">>>>>", "Gps is turned off!! ");*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void checkAnnomalie(View view) {
        type = 3;
        annomalie_btn.setImageDrawable(getResources().getDrawable(R.drawable.chek_on));
        annomalies.setTextColor(getResources().getColor(R.color.color_white));
        if (stopcards != null && stopcard_btn != null) {
            stopcards.setTextColor(getResources().getColor(R.color.color_bleu_fance));
            stopcard_btn.setImageDrawable(getResources().getDrawable(R.drawable.chek_off));
        }

        if (bestpractices != null && bonne_pratique_btn != null) {
            bestpractices.setTextColor(getResources().getColor(R.color.orange_fance));
            bonne_pratique_btn.setImageDrawable(getResources().getDrawable(R.drawable.chek_off));
        }

    }

    public void checkStopcard(View view) {

        type = 1;
        stopcards.setTextColor(getResources().getColor(R.color.color_white));
        stopcard_btn.setImageDrawable(getResources().getDrawable(R.drawable.chek_on));
        if (annomalies != null && annomalie_btn != null) {
            annomalies.setTextColor(getResources().getColor(R.color.color_boutton_contour));
            annomalie_btn.setImageDrawable(getResources().getDrawable(R.drawable.chek_off));
        }

        if (bestpractices != null && bonne_pratique_btn != null) {
            bestpractices.setTextColor(getResources().getColor(R.color.orange_fance));
            bonne_pratique_btn.setImageDrawable(getResources().getDrawable(R.drawable.chek_off));
        }


    }

    public void checkBP(View view) {

        type = 2;
        bonne_pratique_btn.setImageDrawable(getResources().getDrawable(R.drawable.chek_on));
        bestpractices.setTextColor(getResources().getColor(R.color.color_white));
        if (stopcard_btn != null && stopcards != null) {
            stopcard_btn.setImageDrawable(getResources().getDrawable(R.drawable.chek_off));
            stopcards.setTextColor(getResources().getColor(R.color.color_bleu_fance));
        }
        if (annomalies != null && annomalie_btn != null) {
            annomalie_btn.setImageDrawable(getResources().getDrawable(R.drawable.chek_off));
            annomalies.setTextColor(getResources().getColor(R.color.color_boutton_contour));
        }


    }

    protected static final int REQUEST_CHECK_SETTINGS = 0x1;

    private void displayLocationSettingsRequest() {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Intent intent = new Intent(AddStopCardActivity.this, AddStopCardActivity.class);
                        startActivity(intent);
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Show the dialog by calling startResolutionForResult(), and check the result
                        // in onActivityResult().
                        try {
                            status.startResolutionForResult((Activity) AddStopCardActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }

                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                        break;
                }
            }
        });
    }
}
