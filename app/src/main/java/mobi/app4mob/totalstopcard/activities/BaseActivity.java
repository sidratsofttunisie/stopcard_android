package mobi.app4mob.totalstopcard.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;

import mobi.app4mob.total.stopcard.R;
import mobi.app4mob.totalstopcard.models.User;
import mobi.app4mob.totalstopcard.utils.SessionManager;

public class BaseActivity extends AppCompatActivity {

    SessionManager session;
    User user;
    protected Toolbar toolbar ;

    public void initSession(){
        session = new SessionManager(this);
        user = session.getUserDetails();
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.setHeaderTitle("");
        menu.add(0, v.getId(), 0, getString(R.string.regles_dor));
        menu.add(0, v.getId(), 0, getString(R.string.contact_utiles));
        menu.add(0, v.getId(), 0, getString(R.string.config));
        menu.add(0, v.getId(), 0, getString(R.string.logout));
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle() == getString(R.string.config)) {
            Intent intent = new Intent(this, ProfileActivity.class);
            Bundle b = new Bundle();
            intent.putExtras(b);
            this.startActivity(intent);
        } else if (item.getTitle() == getString(R.string.logout)) {
            logout();
        } else if (item.getTitle() == getString(R.string.regles_dor)) {
            Intent intent = new Intent(this, RulesActivity.class);
            Bundle b = new Bundle();
            intent.putExtras(b);
            this.startActivity(intent);
        }  else if (item.getTitle() == getString(R.string.contact_utiles)) {
            Intent intent = new Intent(this, ContactActivity.class);
            Bundle b = new Bundle();
            intent.putExtras(b);
            this.startActivity(intent);
        } else {
            return false;
        }
        return true;
    }


    public void logout() {
        Log.e(">>>>>",">>>>>");
        this.session.logoutUser();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
        finish();
    }

    public void showMenue(View v) {
        registerForContextMenu(v);
        openContextMenu(v);
        unregisterForContextMenu(v);
        //logout();
    }
}
