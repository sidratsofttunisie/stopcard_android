package mobi.app4mob.totalstopcard.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import mobi.app4mob.total.stopcard.R;
import mobi.app4mob.totalstopcard.adapters.ContactAdapter;
import mobi.app4mob.totalstopcard.adapters.LieuAdapter;
import mobi.app4mob.totalstopcard.models.Contact;
import mobi.app4mob.totalstopcard.models.Lieu;
import mobi.app4mob.totalstopcard.models.User;
import mobi.app4mob.totalstopcard.service.ApiService;
import mobi.app4mob.totalstopcard.service.RestClient;
import mobi.app4mob.totalstopcard.utils.SessionManager;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class ContactActivity extends BaseActivity {
    ContactAdapter contact_adapter;
    ListView list_contact;
    ArrayList<Contact> contactList = new ArrayList<Contact>();
    ProgressDialog prgDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        initSession();

        TextView page_title = (TextView) findViewById(R.id.page_title);
        page_title.setText(getString(R.string.contact_utiles));
        toolbar = (Toolbar)findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        loadContact();
    }




    public void loadContact() {


        //LOADER
        prgDialog = new ProgressDialog(this);
        prgDialog.setMessage(getString(R.string.loading));
        prgDialog.setCancelable(false);
        prgDialog.show();


        ApiService service = RestClient.getClient();
        Call<ArrayList<Contact>> call;
        call = service.getContacts(user.getCountry());

        call.enqueue(new Callback<ArrayList<Contact>>() {
            @Override
            public void onResponse(Response<ArrayList<Contact>> response, Retrofit retrofit) {
                prgDialog.hide();
                contactList = response.body();
                Log.i(">>>>", ">>>" + contactList.size());
                initList();
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(">>>>", ">>>" + t.getMessage().toString());
                prgDialog.hide();
                showAlert(getString(R.string.error_loading));
            }
        });
    }

    private void showAlert(String message) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton(getString(R.string.valider), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                }).show();
    }


    public void initList(){
        contact_adapter = new ContactAdapter(ContactActivity.this, contactList);
        list_contact=(ListView)findViewById(R.id.list_contact);
        list_contact.setAdapter(contact_adapter);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
