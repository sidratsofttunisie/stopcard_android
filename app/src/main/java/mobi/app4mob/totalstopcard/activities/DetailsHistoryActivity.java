package mobi.app4mob.totalstopcard.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import mobi.app4mob.totalstopcard.models.StopCard;
import mobi.app4mob.total.stopcard.R;

public class DetailsHistoryActivity extends BaseActivity {

    StopCard stopCard;
    String type ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_history);

        Bundle bn  = getIntent().getExtras();
        initSession();
        stopCard = (StopCard) bn.getSerializable("stopcard");
        type = bn.getString("type","stopcard");
        initView();
    }
    public void initView(){
        ImageView image = (ImageView) findViewById(R.id.photo);
        Picasso.with(this)
                .load(this.getString(R.string.resourses_url) + "uploads/stopcard/" + stopCard.getImage())
                .placeholder(R.drawable.logo_acc) // optional
                .error(R.drawable.logo_acc)
                .fit()
                .into(image);

        TextView lieu = (TextView) findViewById(R.id.lieu);
        TextView date = (TextView) findViewById(R.id.date);
        TextView type_ = (TextView) findViewById(R.id.type);
        TextView description = (TextView) findViewById(R.id.description);
        TextView action = (TextView) findViewById(R.id.action);
        TextView comment_hse = (TextView) findViewById(R.id.comment_hse);

        TextView action_title = (TextView) findViewById(R.id.action_title);
        TextView description_title = (TextView) findViewById(R.id.description_title);
        TextView comment_title = (TextView) findViewById(R.id.comment_title);

        ImageView cat1 = (ImageView) findViewById(R.id.cat1);
        ImageView cat2 = (ImageView) findViewById(R.id.cat2);
        ImageView cat3 = (ImageView) findViewById(R.id.cat3);
        ImageView cat4 = (ImageView) findViewById(R.id.cat4);
        ImageView cat5 = (ImageView) findViewById(R.id.cat5);
        ImageView cat6 = (ImageView) findViewById(R.id.cat6);
        ImageView cat7 = (ImageView) findViewById(R.id.cat7);
        ImageView cat8 = (ImageView) findViewById(R.id.cat8);
        ImageView cat9 = (ImageView) findViewById(R.id.cat9);
        ImageView cat10 = (ImageView) findViewById(R.id.cat10);
        ImageView cat11 = (ImageView) findViewById(R.id.cat11);
        ImageView cat12 = (ImageView) findViewById(R.id.cat12);

        if (stopCard.getType().contains("1"))
            cat1.setVisibility(View.VISIBLE);
        else
            cat1.setVisibility(View.GONE);

        if (stopCard.getType().contains("2"))
            cat2.setVisibility(View.VISIBLE);
        else
            cat2.setVisibility(View.GONE);

        if (stopCard.getType().contains("3"))
            cat3.setVisibility(View.VISIBLE);
        else
            cat3.setVisibility(View.GONE);

        if (stopCard.getType().contains("4"))
            cat4.setVisibility(View.VISIBLE);
        else
            cat4.setVisibility(View.GONE);

        if (stopCard.getType().contains("5"))
            cat5.setVisibility(View.VISIBLE);
        else
            cat5.setVisibility(View.GONE);

        if (stopCard.getType().contains("6"))
            cat6.setVisibility(View.VISIBLE);
        else
            cat6.setVisibility(View.GONE);

        if (stopCard.getType().contains("7"))
            cat7.setVisibility(View.VISIBLE);
        else
            cat7.setVisibility(View.GONE);

        if (stopCard.getType().contains("8"))
            cat8.setVisibility(View.VISIBLE);
        else
            cat8.setVisibility(View.GONE);

        if (stopCard.getType().contains("9"))
            cat9.setVisibility(View.VISIBLE);
        else
            cat9.setVisibility(View.GONE);

        if (stopCard.getType().contains("10"))
            cat10.setVisibility(View.VISIBLE);
        else
            cat10.setVisibility(View.GONE);

        if (stopCard.getType().contains("11"))
            cat11.setVisibility(View.VISIBLE);
        else
            cat11.setVisibility(View.GONE);

        if (stopCard.getType().contains("12"))
            cat12.setVisibility(View.VISIBLE);
        else
            cat12.setVisibility(View.GONE);



        LinearLayout comment_bloc = (LinearLayout) findViewById(R.id.bloc_comment);

        lieu.setText(stopCard.getLieu());
        if(Locale.getDefault().getDisplayLanguage().equals("English")){
            String d = stopCard.getCreated();
            DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            try {
                Date date1 = formatter.parse(d);
                String newstring = new SimpleDateFormat("MM-dd-yyyy").format(date1);
                date.setText(newstring);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }else
        {
            date.setText(stopCard.getCreated());
        }

        action.setText(stopCard.getAction());
        if(type.equals("annomalie")) {
            type_.setText(getResources().getString(R.string.annomalies));
            type_.setBackgroundColor(getResources().getColor(R.color.primary_color));
            action_title.setBackgroundColor(getResources().getColor(R.color.primary_color));
            description_title.setBackgroundColor(getResources().getColor(R.color.primary_color));
            comment_title.setBackgroundColor(getResources().getColor(R.color.primary_color));
        }
        else if (type.equals("bp")){
            type_.setText(getResources().getString(R.string.bonpratique));
            type_.setBackgroundColor(getResources().getColor(R.color.orange));

            action_title.setBackgroundColor(getResources().getColor(R.color.orange));
            description_title.setBackgroundColor(getResources().getColor(R.color.orange));
            comment_title.setBackgroundColor(getResources().getColor(R.color.orange));
        }
        else if (type.equals("stopcard")) {
            type_.setText(getResources().getString(R.string.stopcard));
            type_.setBackgroundColor(getResources().getColor(R.color.secondary_color));

            action_title.setBackgroundColor(getResources().getColor(R.color.secondary_color));
            description_title.setBackgroundColor(getResources().getColor(R.color.secondary_color));
            comment_title.setBackgroundColor(getResources().getColor(R.color.secondary_color));

        }
        description.setText(stopCard.getDescriptionStopcard());


        if(stopCard.getComment_hse() != null && !stopCard.getComment_hse().isEmpty()){
            comment_hse.setText(stopCard.getComment_hse());
            comment_bloc.setVisibility(View.VISIBLE);
        }


    }
}
