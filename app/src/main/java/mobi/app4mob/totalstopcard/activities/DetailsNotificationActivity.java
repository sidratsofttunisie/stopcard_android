package mobi.app4mob.totalstopcard.activities;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import mobi.app4mob.total.stopcard.R;
import mobi.app4mob.totalstopcard.models.SafetyNotification;
import mobi.app4mob.totalstopcard.models.SafetyNotificationList;
import mobi.app4mob.totalstopcard.models.ServerResponse;
import mobi.app4mob.totalstopcard.service.ApiService;
import mobi.app4mob.totalstopcard.service.RestClient;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class DetailsNotificationActivity extends BaseActivity implements LocationListener {

    private SafetyNotification notification ;
    private TextView notif_corp ;
    ProgressDialog prgDialog;
    private LocationManager locationManager;
    private EditText comment;
    private Location location;
    private Button not_affected_btn ;
    private Button safe_btn ;
    private Button help_btn ;
    private Boolean place_is_loading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        initSession();
        notif_corp= (TextView)findViewById(R.id.notif);
        not_affected_btn = (Button)findViewById(R.id.not_affected_btn);
        safe_btn = (Button)findViewById(R.id.safe_btn);
        help_btn = (Button)findViewById(R.id.help_btn);
        comment = (EditText)findViewById(R.id.comment);
        boolean fromExtra = (boolean) getIntent().getExtras().get("fromExtras");
        setUpNotification(fromExtra);
        getPostion();
    }

    public void setUpNotification(boolean fromExtras){
        if(fromExtras) {
            notification = (SafetyNotification) getIntent().getExtras().getSerializable("notification");
            notif_corp.setText(notification.getNotif());
            comment.setText(notification.getComment());
        }else{
            ApiService service = RestClient.getClient();
            String notif_id = getIntent().getExtras().getString("notif_id");
            int user_id =session.getUserDetails().getId();
            Call<SafetyNotification> call = service.getNotification(user_id,Integer.parseInt(notif_id));
            call.enqueue(new Callback<SafetyNotification>() {
                @Override
                public void onResponse(Response<SafetyNotification> response, Retrofit retrofit) {
                    notification = response.body();
                    notif_corp.setText(notification.getNotif());
                    comment.setText(notification.getComment());
                    Log.e("reposne d", String.valueOf(response.body()));
                }

                @Override
                public void onFailure(Throwable t) {
                    Log.e(">>>>",">>>>proxy activity response message: "+t.getMessage());
                }
            });
        }
        if(notification.getReponse()==1){
            Drawable[] icons  =safe_btn.getCompoundDrawables();
            icons[2].setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
            safe_btn.setCompoundDrawables(null,null,icons[2],null);

        }else if(notification.getReponse()==2){
            Drawable[] icons  =help_btn.getCompoundDrawables();
            icons[2].setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
            help_btn.setCompoundDrawables(null,null,icons[2],null);

        }else if(notification.getReponse()==3){
            Drawable[] icons  =not_affected_btn.getCompoundDrawables();
            icons[2].setColorFilter(getResources().getColor(R.color.color_gris), PorterDuff.Mode.SRC_IN);
            not_affected_btn.setCompoundDrawables(null,null,icons[2],null);

        }

    }
    public void reponse(View view){

        prgDialog = new ProgressDialog(this);
        prgDialog.setMessage(getString(R.string.loading));
        prgDialog.setCancelable(false);
        prgDialog.show();

        ApiService service = RestClient.getClient();
        Log.i(">>>>>",">>>>userid"+user.getId());

        Call<ServerResponse> call = service.postSafetyNotification(user.getId(),notification.getId(),notification.getReponse(),location.getLatitude(),location.getLongitude(),comment.getText().toString());
        call.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Response<ServerResponse> response, Retrofit retrofit) {
                prgDialog.hide();
                Log.e(">>>>",">>>>response message: "+ new Gson().toJson(response.body()));
                ServerResponse reponse = response.body();
                Intent intent = new Intent(DetailsNotificationActivity.this, SafetyCheckListActivity.class);
                startActivity(intent);
            }

            @Override
            public void onFailure(Throwable t) {
                prgDialog.hide();
                Log.e(">>>>",">>>>response message: "+t.getMessage());
                showAlert(getString(R.string.error_loading));

            }
        });
    }
    private void showAlert(String message) {
        new AlertDialog.Builder(this)
                .setTitle("Erreur")
                .setMessage(message)
                .setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                }).show();
    }

    public void safe(View view) {

        Drawable[] icons  =help_btn.getCompoundDrawables();
        icons[2].setColorFilter(getResources().getColor(R.color.color_transparent), PorterDuff.Mode.SRC_IN);
        help_btn.setCompoundDrawables(null,null,icons[2],null);

        icons  =safe_btn.getCompoundDrawables();
        icons[2].setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
        safe_btn.setCompoundDrawables(null,null,icons[2],null);

        icons  =not_affected_btn.getCompoundDrawables();
        icons[2].setColorFilter(getResources().getColor(R.color.color_transparent), PorterDuff.Mode.SRC_IN);
        not_affected_btn.setCompoundDrawables(null,null,icons[2],null);
        notification.setReponse(1);
    }

    public void notaffected(View view) {

        Drawable[] icons  =safe_btn.getCompoundDrawables();
        icons[2].setColorFilter(getResources().getColor(R.color.color_transparent), PorterDuff.Mode.SRC_IN);
        safe_btn.setCompoundDrawables(null,null,icons[2],null);

        icons  =not_affected_btn.getCompoundDrawables();
        icons[2].setColorFilter(getResources().getColor(R.color.color_gris), PorterDuff.Mode.SRC_IN);
        not_affected_btn.setCompoundDrawables(null,null,icons[2],null);

        icons  =help_btn.getCompoundDrawables();
        icons[2].setColorFilter(getResources().getColor(R.color.color_transparent), PorterDuff.Mode.SRC_IN);
        help_btn.setCompoundDrawables(null,null,icons[2],null);
        notification.setReponse(3);
    }

    public void help(View view) {

        Drawable[] icons  =safe_btn.getCompoundDrawables();
        icons[2].setColorFilter(getResources().getColor(R.color.color_transparent), PorterDuff.Mode.SRC_IN);
        safe_btn.setCompoundDrawables(null,null,icons[2],null);

        icons  =help_btn.getCompoundDrawables();
        icons[2].setColorFilter(getResources().getColor(R.color.color_white), PorterDuff.Mode.SRC_IN);
        help_btn.setCompoundDrawables(null,null,icons[2],null);

        icons  =not_affected_btn.getCompoundDrawables();
        icons[2].setColorFilter(getResources().getColor(R.color.color_transparent), PorterDuff.Mode.SRC_IN);
        not_affected_btn.setCompoundDrawables(null,null,icons[2],null);
        notification.setReponse(2);

    }
    public void getPostion() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Boolean isGPSEnabled;
        Boolean isNetworkEnabled;

        // getting GPS status
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        // getting network status
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isGPSEnabled && !isNetworkEnabled) {
            Log.i(">>>","no provider");
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
            Log.i(">>>>>", "Gps is turned off!! ");
            //nothing
        } else {   // Either GPS provider or network provider is enabled

            // if GPS Enabled get lat/long using GPS Services
            if (isGPSEnabled) {
                Log.i(">>>","gps active");
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 1, this);
                if (locationManager != null) {
                    Log.i(">>>","locationmanger != null");
                    this.location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                }

            }// End of if GPS Enabled

            //  get location from Network Provider
            if (isNetworkEnabled) {
                Log.i(">>>","network active");
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 1, this);
                if (locationManager != null) {
                    Log.i(">>>","locationmanger != null");
                    this.location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    Log.e("longitude safety check", String.valueOf(location.getLongitude()));
                    Log.e("latitude safety check", String.valueOf(location.getLatitude()));
                }
            }// End of IF network enabled

        }// End of Either GPS provider or network provider is enabled
    }

    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.i(">>>>>>", "Statut changed!! ");
    }

    @Override
    public void onProviderEnabled(String provider) {

        Log.i(">>>>>>", "Gps is turned on!! ");
    }

    @Override
    public void onProviderDisabled(String provider) {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);

    }
}
