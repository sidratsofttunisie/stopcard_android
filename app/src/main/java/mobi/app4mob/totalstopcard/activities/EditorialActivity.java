package mobi.app4mob.totalstopcard.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.webkit.WebView;

import java.util.Locale;

import mobi.app4mob.total.stopcard.R;
public class EditorialActivity extends BaseActivity {

    private WebView webView ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editorial);
        toolbar = (Toolbar)findViewById(R.id.app_bar) ;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        webView = (WebView)findViewById(R.id.editorial) ;
        String source_url ;
        if(Locale.getDefault().getDisplayLanguage().equals("English"))
            source_url= getResources().getString(R.string.server_url)+"editorial?lg=en" ;
        else if(Locale.getDefault().getDisplayLanguage().equals("español"))
            source_url= getResources().getString(R.string.server_url)+"editorial?lg=es" ;
        else
            source_url= getResources().getString(R.string.server_url)+"editorial?lg=fr" ;
        webView.loadUrl(source_url);
    }
}
