package mobi.app4mob.totalstopcard.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.gson.Gson;

import java.util.Locale;

import mobi.app4mob.totalstopcard.models.PostResponse;
import mobi.app4mob.totalstopcard.service.ApiService;
import mobi.app4mob.totalstopcard.service.RestClient;
import mobi.app4mob.totalstopcard.utils.EmailValidator;
import mobi.app4mob.total.stopcard.R;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class ForgotPasswordActivity extends Activity {

    EditText emailEdit;
    private ProgressDialog prgDialog;


    public static String SAVE_ERROR = "1";
    public static String USER_INVALID = "2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        initViews();
    }

    private void initViews() {

        emailEdit = (EditText) findViewById(R.id.editEmail);


    }


    public void forgotPassword(View v) {
        String email_patern =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        EmailValidator emailValidator = new EmailValidator();

        if (!emailValidator.validate(emailEdit.getText().toString().trim())) {

           showAlert(getString(R.string.invalid_email));
        } else {
            sendForgotPassword(emailEdit.getText().toString().trim());
        }
    }


    public void sendForgotPassword(String email){

        prgDialog = new ProgressDialog(this);
        prgDialog.setMessage(getString(R.string.loading));
        prgDialog.setCancelable(false);
        prgDialog.show();

        ApiService service = RestClient.getClient();
        Call<PostResponse> call;
        String locale = "fr";
        if(Locale.getDefault().getDisplayLanguage().equals("English"))
            locale="en";
        else if(Locale.getDefault().getDisplayLanguage().equals("español"))
            locale="es";
        call = service.forgotpassword(email,locale);

        call.enqueue(new Callback<PostResponse>() {

            @Override
            public void onResponse(Response<PostResponse> response, Retrofit retrofit) {
                prgDialog.hide();
                if (response.isSuccess()) {

                    PostResponse result = response.body();
                    Log.e("MainActivity", "response = " + new Gson().toJson(result));
                    if (result.getMessage().equals(ApiService.SUCCESS)) {

                        showAlert(getString(R.string.password_changed));

                    }

                    if (result.getMessage().equals(ApiService.ERROR)) {
                        if(result.getData().equals(USER_INVALID)){
                            showAlert(getString(R.string.mail_doesnt_existe));
                        }
                        else {
                            showAlert(getString(R.string.error_loading));
                        }
                    }


                } else {

                    Log.e("result >>>", "______fail  " + response.code());
                    showAlert(getString(R.string.error_loading));
                }

            }

            @Override
            public void onFailure(Throwable t) {
                prgDialog.hide();
                showAlert(getString(R.string.error_loading));
                Log.e("retro", "onFailure : " + t.toString());
                //  Log.e("tag", t.getLocalizedMessage());

            }
        });

    }
    private void showAlert(String message) {
        new AlertDialog.Builder(this)
                //.setTitle("Erreur")
                .setMessage(message)
                .setPositiveButton(getString(R.string.valider), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                        startActivity(intent); // continue with delete
                    }
                }).show();
    }


}
