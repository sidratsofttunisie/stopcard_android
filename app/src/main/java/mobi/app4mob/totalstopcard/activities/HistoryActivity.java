package mobi.app4mob.totalstopcard.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.ArrayList;

import mobi.app4mob.totalstopcard.adapters.HistoryAdapter;
import mobi.app4mob.totalstopcard.models.StopCard;
import mobi.app4mob.totalstopcard.models.StopCardHistory;
import mobi.app4mob.totalstopcard.service.ApiService;
import mobi.app4mob.totalstopcard.service.RestClient;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import mobi.app4mob.total.stopcard.R;

public class HistoryActivity extends BaseActivity implements OnItemClickListener {
    ListView list;
    HistoryAdapter listAdapter;
    Context context;
    StopCardHistory history = new StopCardHistory();
    ProgressDialog prgDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        toolbar = (Toolbar)findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initSession();

        context = this;
        getHistory();
    }

    public void initViews() {
        list = (ListView) findViewById(R.id.list);
        if (history!=null){
            listAdapter = new HistoryAdapter(context, history.getStopcards(),1);
            list.setAdapter(listAdapter);
            list.setOnItemClickListener(this);
        }

    }

    public void getHistory() {
        //LOADER
        prgDialog = new ProgressDialog(this);
        prgDialog.setMessage(getString(R.string.loading));
        prgDialog.setCancelable(false);
        prgDialog.show();

        ApiService service = RestClient.getClient();

        Log.i(">>>>>",">>>>userid"+user.getId());
        Call<StopCardHistory> call = service.getHistory(user.getId());
        call.enqueue(new Callback<StopCardHistory>() {
            @Override
            public void onResponse(Response<StopCardHistory> response, Retrofit retrofit) {
                prgDialog.hide();
                Log.d(">>>>",">>>>response message: "+ new Gson().toJson(response.body()));
                history = response.body();
                initViews();
            }

            @Override
            public void onFailure(Throwable t) {
                prgDialog.hide();
                Log.e(">>>>",">>>>response message: "+t.getMessage());
                showAlert(getString(R.string.error_loading));

            }
        });
    }

    private void showAlert(String message) {
        new AlertDialog.Builder(this)
                .setTitle("Erreur")
                .setMessage(message)
                .setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                }).show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            StopCard stopCard = history.getStopcards().get(position);
        Intent intent = new Intent(HistoryActivity.this, DetailsHistoryActivity.class);
        intent.putExtra("stopcard", stopCard);
        intent.putExtra("type", "stopcard");
        context.startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

   }
