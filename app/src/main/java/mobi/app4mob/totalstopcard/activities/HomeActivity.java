package mobi.app4mob.totalstopcard.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;

import mobi.app4mob.total.stopcard.R;

import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;

import mobi.app4mob.totalstopcard.models.PostUserResponse;
import mobi.app4mob.totalstopcard.models.User;
import mobi.app4mob.totalstopcard.service.ApiService;
import mobi.app4mob.totalstopcard.service.RestClient;
import mobi.app4mob.totalstopcard.utils.SessionManager;
import mobi.app4mob.totalstopcard.utils.Utils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


public class HomeActivity extends BaseActivity {


    TextView nbStopCard;
    TextView nbAnnomalies;
    TextView nbBonnePratiques;

    private Toolbar toolbar;
    LinearLayout anomalyLayout ,bestPracticeLayout,stopCardLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initSession();
        getStatistique();
        Utils.checkPermissions(this);

        if (user.getHas_annomalies() || user.getHas_bonnes_pratiques() || user.getHas_stop_card() ){
            setContentView(R.layout.activity_home_v3);

            if (!user.getHas_annomalies()){
                anomalyLayout = findViewById(R.id.annomalies_layout);
                anomalyLayout.setVisibility(View.GONE);
            }
            if (!user.getHas_bonnes_pratiques()){
                bestPracticeLayout = findViewById(R.id.bonnes_pratiques_layout);
                bestPracticeLayout.setVisibility(View.GONE);
            }
            if (!user.getHas_stop_card()){
                stopCardLayout = findViewById(R.id.stopcard_layout);
                stopCardLayout.setVisibility(View.GONE);
            }
        }
        else
            setContentView(R.layout.activity_home);

        toolbar = (Toolbar) findViewById(R.id.app_bar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");

        NavigationDrawerFragment navigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer_fragment);
        navigationDrawerFragment.setUp((DrawerLayout) findViewById(R.id.drawer_layout), toolbar, user.getHas_notif());

    }

    private void getStatistique() {

        ApiService service = RestClient.getClient();
        Call<PostUserResponse> call;
        call = service.getUserInformation(user.getId());
        call.enqueue(new Callback<PostUserResponse>() {
            @Override
            public void onResponse(Response<PostUserResponse> response, Retrofit retrofit) {
                if (response.isSuccess()) {

                    PostUserResponse result = response.body();
                    if (result.getMessage().equals(ApiService.SUCCESS)) {
                        User u = result.getData();
                        Log.e("stat", String.valueOf(u.getHas_notif()));
                        session.updateSession(u);
                        try{
                            initView();
                        }catch (Exception e)
                        {
                            Crashlytics.logException(e);
                        }
                    }

                } else {
                    Log.e("result >>>", "______fail  " + response.code());
                }

            }

            @Override
            public void onFailure(Throwable t) {

                Log.e("retro", "onFailure : " + t.toString());
                //  Log.e("tag", t.getLocalizedMessage());

            }
        });

//        initView();
    }

    @SuppressLint("ResourceType")
    private void initView() {
        View view = this.findViewById(android.R.id.content);
        user = session.getUserDetails();
        nbStopCard = (TextView) findViewById(R.id.nb_stop_card);
        nbStopCard.setText(user.getNbStopCards() + "");
        if ( user.getHas_annomalies() || user.getHas_bonnes_pratiques()) {

            if (user.getHas_annomalies()){
                nbAnnomalies = (TextView) findViewById(R.id.nb_annomalies);
                nbAnnomalies.setText(user.getNbAnnomalies() + "");
            }
            if (user.getHas_bonnes_pratiques()){
                nbBonnePratiques = (TextView) findViewById(R.id.nb_bonnes_pratiques);
                nbBonnePratiques.setText(user.getNbBonPratiques() + "");
            }
        }

    }

    public void addStopCard(View v) {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Boolean isGPSEnabled;
        Boolean isNetworkEnabled;

        // getting GPS status
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        // getting network status
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Utils.checkPermissions(HomeActivity.this);
        } else {
            if (!isGPSEnabled && !isNetworkEnabled) {
                displayLocationSettingsRequest();
            } else {
                Intent intent = new Intent(HomeActivity.this, AddStopCardActivity.class);
                startActivity(intent);
            }

        }
    }

    protected static final int REQUEST_CHECK_SETTINGS = 0x1;

    private void displayLocationSettingsRequest() {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Intent intent = new Intent(HomeActivity.this, AddStopCardActivity.class);
                        startActivity(intent);
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Show the dialog by calling startResolutionForResult(), and check the result
                        // in onActivityResult().
                        try {
                            status.startResolutionForResult((Activity) HomeActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }

                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                        break;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CHECK_SETTINGS) {
                Intent intent = new Intent(HomeActivity.this, AddStopCardActivity.class);
                startActivity(intent);
            }

        } else {
            Toast.makeText(HomeActivity.this, getString(R.string.enable_gps), Toast.LENGTH_LONG).show();
        }
    }

    public void lastStopCard(View v) {
        Intent intent;
        if (user.getCan_see_all()) {
            if (user.getHas_annomalies() || user.getHas_bonnes_pratiques()) {
                intent = new Intent(HomeActivity.this, LastMultiStopCardActivity.class);
                startActivity(intent);
            } else {
                intent = new Intent(HomeActivity.this, LastStopCardActivity.class);
                startActivity(intent);
            }
        }
    }

    public void goToAnnomaliesHistory(View v) {
        Intent intent;
        if (user.getHas_annomalies() || user.getHas_bonnes_pratiques()) {
            intent = new Intent(HomeActivity.this, HistoryStopCardActivity.class);
            intent.putExtra("type", "0");
        } else
            intent = new Intent(HomeActivity.this, HistoryActivity.class);
        startActivity(intent);
    }

    public void goToStopCardHistory(View v) {
        Intent intent;
        if (user.getHas_annomalies() || user.getHas_bonnes_pratiques()) {
            intent = new Intent(HomeActivity.this, HistoryStopCardActivity.class);
            intent.putExtra("type", "1");
        } else
            intent = new Intent(HomeActivity.this, HistoryActivity.class);
        startActivity(intent);
    }

    public void goToBPHistory(View v) {
        Intent intent;
        if (user.getHas_annomalies() || user.getHas_bonnes_pratiques()) {
            intent = new Intent(HomeActivity.this, HistoryStopCardActivity.class);
            intent.putExtra("type", "2");
        } else
            intent = new Intent(HomeActivity.this, HistoryActivity.class);
        startActivity(intent);
    }


    @Override
    protected void onResume() {
        getStatistique();
        super.onResume();
    }
}

