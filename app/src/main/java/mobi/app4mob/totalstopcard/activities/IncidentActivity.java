package mobi.app4mob.totalstopcard.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import mobi.app4mob.totalstopcard.adapters.IncidentAdapter;
import mobi.app4mob.total.stopcard.R;

public class IncidentActivity extends BaseActivity {
    int[] selected_incident = new int[12];

    IncidentAdapter adapter;
    ListView list;
    String[] web;
    Integer[] imageId = {
            R.drawable.cat1,
            R.drawable.cat2,
            R.drawable.cat3,
            R.drawable.cat4,
            R.drawable.cat5,
            R.drawable.cat6,
            R.drawable.cat7,
            R.drawable.cat8,
            R.drawable.cat9,
            R.drawable.cat10,
            R.drawable.cat11,
            R.drawable.cat12,

    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incident);
        initSession();
        TextView page_title = (TextView) findViewById(R.id.page_title);
        page_title.setText(getString(R.string.type_incident));

        String[] web_ = {
                getString(R.string.regle_1),
                getString(R.string.regle_2),
                getString(R.string.regle_3),
                getString(R.string.regle_4),
                getString(R.string.regle_5),
                getString(R.string.regle_6),
                getString(R.string.regle_7),
                getString(R.string.regle_8),
                getString(R.string.regle_9),
                getString(R.string.regle_10),
                getString(R.string.regle_11),
                getString(R.string.regle_12),

        } ;

        web = web_;

        Bundle bn = new Bundle();
        bn = getIntent().getExtras();

        selected_incident = (int[]) bn.getSerializable("selected_incident");


        adapter = new
                IncidentAdapter(IncidentActivity.this, web, imageId, selected_incident);
        list=(ListView)findViewById(R.id.list);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                //  Toast.makeText(MainActivity.this, "You Clicked at " +web[+ position], Toast.LENGTH_SHORT).show();

            }
        });

    }

    public void submit(View v){
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result",adapter.getSelectedIncident());
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }
}
