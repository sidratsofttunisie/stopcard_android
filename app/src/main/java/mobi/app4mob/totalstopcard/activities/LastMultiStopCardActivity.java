package mobi.app4mob.totalstopcard.activities;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;

import mobi.app4mob.total.stopcard.R;
import mobi.app4mob.totalstopcard.adapters.HistoryAdapter;
import mobi.app4mob.totalstopcard.adapters.PagerAdapter;
import mobi.app4mob.totalstopcard.models.StopCard;
import mobi.app4mob.totalstopcard.models.StopCardHistory;
import mobi.app4mob.totalstopcard.service.ApiService;
import mobi.app4mob.totalstopcard.service.RestClient;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class LastMultiStopCardActivity extends BaseActivity  {
    StopCardHistory history = new StopCardHistory();
    TabLayout tabLayout ;
    ProgressDialog prgDialog;
    private Button tabTextView ,tabTextView2,tabTextView3 ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_stop_card);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);

        toolbar = (Toolbar)findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        RelativeLayout relativeLayout2 = (RelativeLayout)
                LayoutInflater.from(this).inflate(R.layout.tab_layout, tabLayout, false);
        tabTextView2 = (Button) relativeLayout2.findViewById(R.id.tab_title);
        tabTextView2.setText(getString(R.string.annomalies));
        tabLayout.addTab(tabLayout.newTab().setCustomView(relativeLayout2));

        RelativeLayout relativeLayout3 = (RelativeLayout)
                LayoutInflater.from(this).inflate(R.layout.tab_layout, tabLayout, false);
        tabTextView3 = (Button) relativeLayout3.findViewById(R.id.tab_title);
        tabTextView3.setText("Stop Card");
        tabLayout.addTab(tabLayout.newTab().setCustomView(relativeLayout3));

        RelativeLayout relativeLayout = (RelativeLayout)
                LayoutInflater.from(this).inflate(R.layout.tab_layout_without_border, tabLayout, false);
        tabTextView = (Button) relativeLayout.findViewById(R.id.tab_title);
        tabTextView.setText(getString(R.string._bonpratique));

        tabLayout.addTab(tabLayout.newTab().setCustomView(relativeLayout));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        initSession();
        getHistory();
    }

    public void getHistory() {
        //LOADER
        prgDialog = new ProgressDialog(this);
        prgDialog.setMessage(getString(R.string.loading));
        prgDialog.setCancelable(false);
        prgDialog.show();

        ApiService service = RestClient.getClient();

        Log.i(">>>>>",">>>>userid"+user.getId());
        Call<StopCardHistory> call = service.getLastStopCard(user.getCountry());
        call.enqueue(new Callback<StopCardHistory>() {
            @Override
            public void onResponse(Response<StopCardHistory> response, Retrofit retrofit) {
                prgDialog.hide();
                Log.d(">>>>",">>>>response message: "+ new Gson().toJson(response.body()));
                history = response.body();
                initViews();
            }

            @Override
            public void onFailure(Throwable t) {
                prgDialog.hide();
                Log.e(">>>>",">>>>response message: "+t.getMessage());
                showAlert(getString(R.string.error_loading));

            }
        });
    }
    private void showAlert(String message) {
        new AlertDialog.Builder(this)
                .setTitle("Erreur")
                .setMessage(message)
                .setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                }).show();
    }
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void initViews(){

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount(),history);
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                for (int i = 0; i < tabLayout.getTabCount(); i++) {
                    if (i == tab.getPosition()) {
                        ((Button)tabLayout.getTabAt(i).getCustomView().findViewById(R.id.tab_title)).setTextColor(getResources().getColor(R.color.color_bleu_ciel));
                        if(i==0)
                            (tabLayout.getTabAt(i).getCustomView().findViewById(R.id.tab_title)).setBackground(getResources().getDrawable(R.drawable.left_tab));
                        else if(i==2)
                            (tabLayout.getTabAt(i).getCustomView().findViewById(R.id.tab_title)).setBackground(getResources().getDrawable(R.drawable.right_tab));
                        else
                            (tabLayout.getTabAt(i).getCustomView().findViewById(R.id.tab_title)).setBackgroundColor(getResources().getColor(R.color.color_white));
                    } else {
                        ((Button)tabLayout.getTabAt(i).getCustomView().findViewById(R.id.tab_title)).setTextColor(getResources().getColor(R.color.color_white));
                        (tabLayout.getTabAt(i).getCustomView().findViewById(R.id.tab_title)).setBackgroundColor(getResources().getColor(R.color.color_transparent));

                    }

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

            viewPager.setCurrentItem(0);
            ((Button)tabLayout.getTabAt(0).getCustomView().findViewById(R.id.tab_title)).setTextColor(getResources().getColor(R.color.color_bleu_ciel));
            (tabLayout.getTabAt(0).getCustomView().findViewById(R.id.tab_title)).setBackground(getResources().getDrawable(R.drawable.left_tab));

        tabTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(2);
            }
        });
        tabTextView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(0);
            }
        });
        tabTextView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(1);
            }
        });
    }
   }
