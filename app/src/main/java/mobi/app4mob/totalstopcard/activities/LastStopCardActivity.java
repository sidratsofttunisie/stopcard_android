package mobi.app4mob.totalstopcard.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;

import mobi.app4mob.total.stopcard.R;
import mobi.app4mob.totalstopcard.adapters.HistoryAdapter;
import mobi.app4mob.totalstopcard.models.StopCard;
import mobi.app4mob.totalstopcard.models.StopCardHistory;
import mobi.app4mob.totalstopcard.models.User;
import mobi.app4mob.totalstopcard.service.ApiService;
import mobi.app4mob.totalstopcard.service.RestClient;
import mobi.app4mob.totalstopcard.utils.SessionManager;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class LastStopCardActivity extends BaseActivity {
    ListView list;
    HistoryAdapter listAdapter;
    Context context;
    StopCardHistory history = new StopCardHistory();
    ProgressDialog prgDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        toolbar = (Toolbar)findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initSession();
        getHistory();
        context = this;

    }

    public void initViews() {
        ((TextView) findViewById(R.id.page_title)).setText(getString(R.string.last_stopcard));
        list = (ListView) findViewById(R.id.list);
        listAdapter = new HistoryAdapter(context, history.getStopcards(),1);
        list.setAdapter(listAdapter);
        list.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    StopCard stopCard = history.getStopcards().get(position);
                    System.out.println(stopCard!=null);
                    Intent intent = new Intent(LastStopCardActivity.this, DetailsHistoryActivity.class);
                    intent.putExtra("stopcard", stopCard);
                    startActivity(intent);
                }catch (Exception e){
                    Log.e("last",e.getMessage());
                }

            }
        });
    }

    public void getHistory() {
        //LOADER
        prgDialog = new ProgressDialog(this);
        prgDialog.setMessage(getString(R.string.loading));
        prgDialog.setCancelable(false);
        prgDialog.show();

        ApiService service = RestClient.getClient();

        Call<StopCardHistory> call = service.getLastStopCard(user.getCountry());
        call.enqueue(new Callback<StopCardHistory>() {
            @Override
            public void onResponse(Response<StopCardHistory> response, Retrofit retrofit) {
                prgDialog.hide();
              //  Log.d(">>>>",">>>>response body"+response.body().toString());
                Log.d(">>>>",">>>>response message: "+response.message().toString());
                history= response.body();
                initViews();
            }

            @Override
            public void onFailure(Throwable t) {
                prgDialog.hide();
                Log.e("lastStopCardActivity" ,t.getMessage());
                showAlert(getString(R.string.error_loading));

            }
        });
    }

    private void showAlert(String message) {
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.error))
                .setMessage(message)
                .setPositiveButton(getString(R.string.valider), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                }).show();
    }

   }
