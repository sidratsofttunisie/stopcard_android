package mobi.app4mob.totalstopcard.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import mobi.app4mob.totalstopcard.adapters.LieuAdapter;
import mobi.app4mob.totalstopcard.models.Lieu;
import mobi.app4mob.total.stopcard.R;
public class LieuActivity extends BaseActivity {
    LieuAdapter lieu_adapter;
    ListView list_lieu;
   ArrayList<Lieu> lieux = new ArrayList<Lieu>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lieu);
        initSession();
        Bundle bn = new Bundle();
        bn = getIntent().getExtras();

        TextView page_title = (TextView) findViewById(R.id.page_title);
        page_title.setText(getString(R.string.location));

        lieux = (ArrayList<Lieu>) bn.getSerializable("lieux");

        lieu_adapter = new LieuAdapter(LieuActivity.this, lieux);
        list_lieu=(ListView)findViewById(R.id.list_lieu);
        list_lieu.setAdapter(lieu_adapter);
        list_lieu.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                submit(lieux.get(position).getId(), position);

            }
        });


    }

    public void submit(int id, int position){
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result",id);
        returnIntent.putExtra("position",position);
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }
}
