package mobi.app4mob.totalstopcard.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.gson.Gson;
import com.maksim88.passwordedittext.PasswordEditText;

import mobi.app4mob.totalstopcard.Gcm.GCMRegistrationIntentService;
import mobi.app4mob.totalstopcard.models.PostUserResponse;
import mobi.app4mob.totalstopcard.models.User;
import mobi.app4mob.totalstopcard.service.ApiService;
import mobi.app4mob.totalstopcard.service.RestClient;
import mobi.app4mob.totalstopcard.utils.SessionManager;
import mobi.app4mob.totalstopcard.utils.Utils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import mobi.app4mob.total.stopcard.R;

public class LoginActivity extends Activity {

    EditText usernameEdit;
    PasswordEditText passwordEdit;
    String msgErreur;

    String email;
    String password;
    private ProgressDialog prgDialog;

    SessionManager session;
    //Creating a broadcast receiver for gcm registration
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    String regId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Utils.checkPermissions(this);
        session = new SessionManager(this);
        if (session.isLoggedIn() && session.withLongSession()) {
            goToHomePage();
        }
        initView();
        if (session.getRegToken().isEmpty()) {
            setRegId();
        } else {
            regId = session.getRegToken();
        }
    }

    public void setRegId() {
        //Checking play service is available or not
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());

        //if play service is not available
        if (ConnectionResult.SUCCESS != resultCode) {
            //If play service is supported but not installed
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                //Displaying message that play service is not installed
                Toast.makeText(getApplicationContext(), "Google Play Service is not install/enabled in this device!", Toast.LENGTH_LONG).show();
                GooglePlayServicesUtil.showErrorNotification(resultCode, getApplicationContext());

                //If play service is not supported
                //Displaying an error message
            } else {
                Toast.makeText(getApplicationContext(), "This device does not support for Google Play Service!", Toast.LENGTH_LONG).show();
            }

            //If play service is available
        } else {
            //Starting intent to register device
            Intent itent = new Intent(this, GCMRegistrationIntentService.class);
            startService(itent);
        }

        //Initializing our broadcast receiver
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {

            //When the broadcast received
            //We are sending the broadcast from GCMRegistrationIntentService

            @Override
            public void onReceive(Context context, Intent intent) {
                //If the broadcast has received with success
                //that means device is registered successfully
                if (intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_SUCCESS)) {
                    //Getting the registration token from the intent
                    String token = intent.getStringExtra("token");
                    //Displaying the token as toast
                    Toast.makeText(getApplicationContext(), "Registration token:" + token, Toast.LENGTH_LONG).show();
                    session.setRegToken(token);
                    //if the intent is not with success then displaying error messages
                } else if (intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_ERROR)) {
                    Toast.makeText(getApplicationContext(), "GCM registration error!", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Error occurred", Toast.LENGTH_LONG).show();
                }
            }
        };
    }
   /* public void connect(View v) {
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }*/

    public void initView() {

        usernameEdit = (EditText) findViewById(R.id.username);
        passwordEdit = (PasswordEditText) findViewById(R.id.password);
    }

    public void connect(View v) {


        boolean verify = true;
        msgErreur = "";


        String email_patern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        email_patern = "^[a-z0-9_-]{3,15}$";

        email = usernameEdit.getText().toString().trim();
        password = passwordEdit.getText().toString().trim();

        if (email.matches("")) {
            msgErreur += getString(R.string.invalid_username) + "\n";
            verify = false;
            usernameEdit.setBackgroundResource(R.drawable.invalid_champ);
        } else {
            usernameEdit.setBackgroundResource(R.drawable.text_fond);
        }


        if (verify) {
            connect();
        } else {
            showMessage(msgErreur);
        }


    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if ( prgDialog!=null && prgDialog.isShowing() ){
            prgDialog.cancel();
            prgDialog.dismiss();
        }
    }
    public void connect() {
        prgDialog = new ProgressDialog(this);
        prgDialog.setMessage(getString(R.string.loading));
        prgDialog.setCancelable(false);
        prgDialog.show();

        ApiService service = RestClient.getClient();
        Call<PostUserResponse> call;
        call = service.login(email, password, session.getRegToken());
        call.enqueue(new Callback<PostUserResponse>() {

            @Override
            public void onResponse(Response<PostUserResponse> response, Retrofit retrofit) {
                prgDialog.hide();

                if (response.isSuccess()) {

                    PostUserResponse result = response.body();
                    User user = result.getData();
                    Log.e("MainActivity", "response = " + new Gson().toJson(response.body()));
                    if (result.getMessage().equals(ApiService.SUCCESS)) {

                        authenticateUser(user);
                    }

                    if (result.getMessage().equals(ApiService.ERROR)) {

                        showMessage(getString(R.string.wrong_password));
                    }


                } else {
                    Log.e("result >>>", "______fail  " + response.code());
                    showMessage(getString(R.string.error_loading));
                }

            }

            @Override
            public void onFailure(Throwable t) {
                prgDialog.hide();
                showMessage(getString(R.string.error_loading));
                Log.e("retro", "onFailure : " + t.toString());
                //  Log.e("tag", t.getLocalizedMessage());

            }
        });
    }

    public void goToHomePage() {
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    public void showMessage(String message) {
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.error))
                .setMessage(message)
                .setPositiveButton(getString(R.string.valider), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                }).show();
    }

    private void authenticateUser(User user) {
        // create new sesssion with this user
        session.createLoginSession(user, true);
        goToHomePage();
    }

    public void forgotPassword(View v) {
        Log.i(">>>>", ">>>>> forgot password clicked");
        Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
        startActivity(intent);

    }

}