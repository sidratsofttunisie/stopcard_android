package mobi.app4mob.totalstopcard.activities;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import mobi.app4mob.total.stopcard.R;
import mobi.app4mob.totalstopcard.adapters.NavDrawerListAdapter;
import mobi.app4mob.totalstopcard.utils.SessionManager;
import mobi.app4mob.totalstopcard.widget.NavDrawerItem;


/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationDrawerFragment extends Fragment {

    private ListView mDrawerList;
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;
    private DrawerLayout mDrawerLayout ;
    private android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;
    private boolean hasNotif ;
    public NavigationDrawerFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        mDrawerList = (ListView)view.findViewById(R.id.navigation_drawer_menu);
        return view ;
    }
    public void setUp(DrawerLayout drawerLayout,Toolbar toolbar,Boolean hasNotif){
        this.mDrawerLayout = drawerLayout ;
        this.hasNotif=hasNotif;
        mDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(getActivity(),
                mDrawerLayout,
                toolbar,
                R.string.navigation_drawer_open, // nav drawer open - description for accessibility
                R.string.navigation_drawer_close // nav drawer close - description for accessibility
        ) {
            public void onDrawerClosed(View view) {
                getActivity().invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getActivity().invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        }) ;
        setUpNavigationDrawer();
    }

    @SuppressLint("ResourceType")
    private void setUpNavigationDrawer(){
        navDrawerItems = new ArrayList<NavDrawerItem>();
        Log.e("nav notif",String.valueOf(this.hasNotif));
        if(this.hasNotif){
            navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items_usa);
            // nav drawer icons from resources
            navMenuIcons = getResources()
                    .obtainTypedArray(R.array.nav_drawer_icons_usa);
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));
            navMenuIcons.recycle();
        }else {
            navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items_v3);
            // nav drawer icons from resources
            navMenuIcons = getResources()
                    .obtainTypedArray(R.array.nav_drawer_icons_v3);
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));

            navMenuIcons.recycle();
        }

        adapter = new NavDrawerListAdapter(getContext(),
                navDrawerItems);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);
                ((TextView)view.findViewById(R.id.title)).setTextColor(getResources().getColor(R.color.secondary_color));
                swtich(position);
                for(int i=0 ;i<parent.getCount();i++)
                    if(i!=position){
                        ((TextView)parent.getChildAt(i).findViewById(R.id.title)).setTextColor(getResources().getColor(R.color.text_gray));
                    }
                mDrawerLayout.closeDrawers();
            }
        });

    }

    private void swtich(int pos ){
        if(!this.hasNotif){
            switch (pos){
                case 0:
                    Intent intent5= new Intent(getContext(), EditorialActivity.class);
                    Bundle b5 = new Bundle();
                    intent5.putExtras(b5);
                    getActivity().startActivity(intent5);
                    break;
                case 1:
                    Intent intent2 = new Intent(getContext(), RulesActivity.class);
                    Bundle b2 = new Bundle();
                    intent2.putExtras(b2);
                    getActivity().startActivity(intent2);
                    break;
                case 2:
                    Intent intent1 = new Intent(getContext(), ContactActivity.class);
                    Bundle b1 = new Bundle();
                    intent1.putExtras(b1);
                    getActivity().startActivity(intent1);
                    break;
                case 3:
                    Intent intent = new Intent(getContext(), ProfileActivity.class);
                    Bundle b = new Bundle();
                    intent.putExtras(b);
                    getActivity().startActivity(intent);break;
                case 4:
                    SessionManager session = new SessionManager(getContext());
                    session.logoutUser();
                    Intent intent3 = new Intent(getContext(), LoginActivity.class);
                    intent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent3.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getActivity().startActivity(intent3);
                    getActivity().finish();
            }
        }else{
            switch (pos){
                case 0:
                    Intent intent5 = new Intent(getContext(), SafetyCheckListActivity.class);
                    Bundle b5 = new Bundle();
                    intent5.putExtras(b5);
                    getActivity().startActivity(intent5);
                    break;
                case 1:
                    Intent intent6 = new Intent(getContext(), EditorialActivity.class);
                    Bundle b6 = new Bundle();
                    intent6.putExtras(b6);
                    getActivity().startActivity(intent6);
                    break;
                case 2:
                    Intent intent2 = new Intent(getContext(), RulesActivity.class);
                    Bundle b2 = new Bundle();
                    intent2.putExtras(b2);
                    getActivity().startActivity(intent2);
                    break;
                case 3:
                    Intent intent1 = new Intent(getContext(), ContactActivity.class);
                    Bundle b1 = new Bundle();
                    intent1.putExtras(b1);
                    getActivity().startActivity(intent1);
                    break;
                case 4:
                    Intent intent = new Intent(getContext(), ProfileActivity.class);
                    Bundle b = new Bundle();
                    intent.putExtras(b);
                    getActivity().startActivity(intent);break;
                case 5:
                    SessionManager session = new SessionManager(getContext());
                    session.logoutUser();
                    Intent intent3 = new Intent(getContext(), LoginActivity.class);
                    intent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent3.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getActivity().startActivity(intent3);
                    getActivity().finish();
            }
        }
    }

}
