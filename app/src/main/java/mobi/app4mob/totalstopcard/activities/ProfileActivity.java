package mobi.app4mob.totalstopcard.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.andreabaccega.widget.FormEditText;
import com.google.gson.Gson;

import mobi.app4mob.total.stopcard.R;
import mobi.app4mob.totalstopcard.models.PostResponse;
import mobi.app4mob.totalstopcard.models.User;
import mobi.app4mob.totalstopcard.service.ApiService;
import mobi.app4mob.totalstopcard.service.RestClient;
import mobi.app4mob.totalstopcard.utils.SessionManager;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class ProfileActivity extends BaseActivity {

    EditText email_edit;
    EditText name_edit;
    EditText prenom_edit;
    EditText password_edit;
    EditText password_repeate_edit;

    String name;
    String prenom;
    String username;
    String password;

    public static String USERNAME_EXISTE = "1";
    public static String USER_INVALID = "2";
    public static String SAVE_ERROR = "3";
    private ProgressDialog prgDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        toolbar = (Toolbar)findViewById(R.id.app_bar);
        System.out.println(toolbar==null);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        initSession();

        initView();

        initValues();


    }
    public void initView(){
        email_edit = (EditText) findViewById(R.id.edit_email);
        name_edit = (EditText) findViewById(R.id.edit_name);
        prenom_edit = (EditText) findViewById(R.id.edit_prenom);
        password_edit = (EditText) findViewById(R.id.edit_password);
       // password_repeate_edit = (EditText) findViewById(R.id.edit_repeate_password);
    }

    public void initValues(){
        name_edit.setText(user.getNom());
        prenom_edit.setText(user.getPrenom());
        email_edit.setText(user.getUsername());
        name_edit.setText(user.getNom());
    }

    public void getValues(){
        username = email_edit.getText().toString().trim();
        name = name_edit.getText().toString().trim();
        prenom = prenom_edit.getText().toString().trim();
        password = password_edit.getText().toString().trim();
}


    private boolean formValid() {
        FormEditText[] allFields = { (FormEditText) name_edit, (FormEditText) prenom_edit,  (FormEditText) email_edit};

        boolean allValid = true;
        for (FormEditText field : allFields) {
            allValid = field.testValidity() && allValid;
        }



        if (allValid) {
            return true;
        } else {
            return false;
        }
    }

    public void submit(View v){
        if (formValid()){

            prgDialog = new ProgressDialog(this);
            prgDialog.setMessage(getString(R.string.loading));
            prgDialog.setCancelable(false);
            prgDialog.show();

            ApiService service = RestClient.getClient();
            Call<PostResponse> call;

            getValues();

            Log.e(">>>>","username: "+username+" /// "+"password: "+password+" name: "+name+" prenom:"+prenom);
            call = service.editUser(username,password, name, prenom, user.getId()+"");

            call.enqueue(new Callback<PostResponse>() {

                @Override
                public void onResponse(Response<PostResponse> response, Retrofit retrofit) {
                    prgDialog.hide();
                    if (response.isSuccess()) {

                        PostResponse result = response.body();
                        Log.e("MainActivity", "response = " + new Gson().toJson(result));
                        if (result.getMessage().equals(ApiService.SUCCESS)) {

                            user.setNom(name);
                            user.setPrenom(prenom);
                            user.setUsername(username);
                            session.updateSession(user);
                            finish();
                        }

                        if (result.getMessage().equals(ApiService.ERROR)) {
                            if(result.getData().equals(USERNAME_EXISTE)){
                                showMessage(getString(R.string.username_existe));
                            }
                            else if(result.getData().equals(USER_INVALID)){
                                showMessage(getString(R.string.invalid_user));
                            }
                            else {
                                showMessage(getString(R.string.error_loading));
                            }
                        }


                    } else {

                        Log.e("result >>>", "______fail  " + response.code());
                        showMessage(getString(R.string.error_loading));
                    }

                }

                @Override
                public void onFailure(Throwable t) {
                    prgDialog.hide();
                    showMessage(getString(R.string.error_loading));
                    Log.e("retro", "onFailure : " + t.toString());
                    //  Log.e("tag", t.getLocalizedMessage());

                }
            });

        }
    }

    public void showMessage(String message) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                }).show();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
