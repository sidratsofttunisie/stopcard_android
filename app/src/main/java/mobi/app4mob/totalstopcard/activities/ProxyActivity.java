package mobi.app4mob.totalstopcard.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;

import mobi.app4mob.total.stopcard.R;
import mobi.app4mob.totalstopcard.models.SafetyNotification;
import mobi.app4mob.totalstopcard.models.SafetyNotificationList;
import mobi.app4mob.totalstopcard.models.StopCard;
import mobi.app4mob.totalstopcard.service.ApiService;
import mobi.app4mob.totalstopcard.service.RestClient;
import mobi.app4mob.totalstopcard.utils.SessionManager;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class ProxyActivity extends BaseActivity {
    private SafetyNotification safetyNotification ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initSession();
        ApiService service = RestClient.getClient();
        String notif_id = getIntent().getExtras().getString("notif_id");
        int user_id =session.getUserDetails().getId();
        Log.e("user id", String.valueOf(user_id));
        Log.e("notif",notif_id);
        Call<SafetyNotification> call = service.getNotification(user_id,Integer.parseInt(notif_id));
        call.enqueue(new Callback<SafetyNotification>() {
            @Override
            public void onResponse(Response<SafetyNotification> response, Retrofit retrofit) {

                safetyNotification = response.body();
                Log.e("notification ", String.valueOf(response.body().getNotif()));
                Intent intent = new Intent(ProxyActivity.this, DetailsNotificationActivity.class);
                intent.putExtra("notification", safetyNotification);
                startActivity(intent);
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(">>>>",">>>>proxy activity response message: "+t.getMessage());
            }
        });

    }
}
