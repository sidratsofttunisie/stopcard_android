package mobi.app4mob.totalstopcard.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.LinearLayout;

import mobi.app4mob.total.stopcard.R;
import mobi.app4mob.totalstopcard.adapters.FullScreenImageAdapter;
import mobi.app4mob.totalstopcard.utils.Utils;

public class RulesPagerActivity extends BaseActivity {

    private int current_pos;
    private Utils utils;
    private FullScreenImageAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rules_pager);

        initSession();
        DisplayMetrics metrics;

        metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        utils = new Utils(getApplicationContext());

        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        adapter = new FullScreenImageAdapter(this);

        viewPager.setAdapter(adapter);
        Intent intent = getIntent();
        int position = intent.getIntExtra("position", 0);
        viewPager.setCurrentItem(position + 1);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                Log.d(">>>", "onPageSelected() :: " + "position: " + position);

                // skip fake page (first), go to last page
                if (position == 0) {
                    viewPager.setCurrentItem(12, false);
                }

                // skip fake page (last), go to first page
                if (position == 13) {
                    viewPager.setCurrentItem(1, false); //notice how this jumps to position 1, and not position 0. Position 0 is the fake page!
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


}
