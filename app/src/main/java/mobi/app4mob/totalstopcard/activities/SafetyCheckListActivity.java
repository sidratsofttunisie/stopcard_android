package mobi.app4mob.totalstopcard.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import mobi.app4mob.total.stopcard.R;
import mobi.app4mob.totalstopcard.adapters.HistoryAdapter;
import mobi.app4mob.totalstopcard.adapters.SafetyCheckListAdapter;
import mobi.app4mob.totalstopcard.models.SafetyNotification;
import mobi.app4mob.totalstopcard.models.SafetyNotificationList;
import mobi.app4mob.totalstopcard.models.StopCard;
import mobi.app4mob.totalstopcard.models.StopCardHistory;
import mobi.app4mob.totalstopcard.service.ApiService;
import mobi.app4mob.totalstopcard.service.RestClient;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class SafetyCheckListActivity extends BaseActivity {

    private ListView listView ;
    SafetyNotificationList notifications ;
    private SafetyCheckListAdapter listAdapter ;
    ProgressDialog prgDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_safety_check_list);
        listView = (ListView)findViewById(R.id.safetychecklist) ;
        toolbar = (Toolbar)findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        initSession();
        getHistory();
    }
    public void initViews() {
        listView = (ListView)findViewById(R.id.safetychecklist) ;
        listAdapter = new SafetyCheckListAdapter(getBaseContext(), notifications.getSafetyNotifications());
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SafetyNotification notif = notifications.getSafetyNotifications().get(position);

                    Intent intent = new Intent(SafetyCheckListActivity.this, DetailsNotificationActivity.class);
                    intent.putExtra("notification", notif);
                    intent.putExtra("fromExtras", true);
                    startActivity(intent);
            }
        });
    }

    public void getHistory() {
        //LOADER
        prgDialog = new ProgressDialog(this);
        prgDialog.setMessage(getString(R.string.loading));
        prgDialog.setCancelable(false);
        prgDialog.show();

        ApiService service = RestClient.getClient();

        Log.i(">>>>>",">>>>userid"+user.getId());
        Call<SafetyNotificationList> call = service.getSafetyNotification(user.getId());
        call.enqueue(new Callback<SafetyNotificationList>() {
            @Override
            public void onResponse(Response<SafetyNotificationList> response, Retrofit retrofit) {
                prgDialog.hide();
                Log.d(">>>>",">>>>response message: "+ new Gson().toJson(response.body()));
                notifications = response.body();
                Log.e("response ",new Gson().toJson(notifications));
                initViews();
            }

            @Override
            public void onFailure(Throwable t) {
                prgDialog.hide();
                Log.e(">>>>",">>>>response message: "+t.getMessage());
                showAlert(getString(R.string.error_loading));

            }
        });
    }
    private void showAlert(String message) {
        new AlertDialog.Builder(this)
                .setTitle("Erreur")
                .setMessage(message)
                .setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                }).show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
