package mobi.app4mob.totalstopcard.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.QuickContactBadge;
import android.widget.TextView;

import java.util.ArrayList;

import mobi.app4mob.total.stopcard.R;
import mobi.app4mob.totalstopcard.activities.ContactActivity;
import mobi.app4mob.totalstopcard.models.Contact;
import mobi.app4mob.totalstopcard.models.Lieu;

/**
 * Created by adnen on 01/02/2016.
 */
public class ContactAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;
    Context context;
    ArrayList<Contact> contactList;
    Button button ;

    public ContactAdapter(Context context, ArrayList<Contact> contactList) {

        this.context = context;
        this.contactList = contactList;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return contactList.size();
    }

    @Override
    public Contact getItem(int position) {
        return contactList.get(position);
    }


    @Override
    public long getItemId(int i) {
        return (long) i;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        final ViewHolder holder;
        if (convertView == null) {
            v = inflater.inflate(R.layout.row_contact, parent, false);
            holder = new ViewHolder();
        } else {
            holder = (ViewHolder) v.getTag();
        }
        final Contact contact = this.getItem(position);

        holder.nom = (TextView) v.findViewById(R.id.nom);
        holder.email = (TextView) v.findViewById(R.id.email);
        holder.poste = (TextView) v.findViewById(R.id.poste);
        holder.tel = (TextView) v.findViewById(R.id.tel);
        holder.email_btn = (ImageView) v.findViewById(R.id.email_btn);
        holder.call_btn = (ImageView) v.findViewById(R.id.call_btn);

        holder.nom.setText(contact.getNom());

        String tels = "";
        for (String tel : contact.getTel()) {
            tels += ", " + tel;
        }
        if (!tels.isEmpty()) {
            holder.call_btn.setVisibility(View.VISIBLE);
            holder.tel.setVisibility(View.VISIBLE);
            holder.tel.setText(tels.substring(1));
        }else{
            holder.call_btn.setVisibility(View.GONE);
            holder.tel.setVisibility(View.GONE);
        }
        if (contact.getPoste() != null) {
            holder.poste.setText(contact.getPoste());
            holder.poste.setVisibility(View.VISIBLE);
        } else {
            holder.poste.setText("");
            holder.poste.setVisibility(View.GONE);
        }
        holder.call_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(contact.getNom(),contact.getTel());
            }
        });

        if(contact.getEmail()!=""){
            holder.email.setText(contact.getEmail());
            holder.email_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent testIntent = new Intent(Intent.ACTION_VIEW);
                    Uri data = Uri.parse("mailto:?to=" +contact.getEmail());
                    testIntent.setData(data);
                    context.startActivity(testIntent);
                }
            });
        }
        if (contact.getEmail() == null || contact.getEmail().equals("")) {
            holder.email_btn.setVisibility(View.GONE);
        }else{
            holder.email_btn.setVisibility(View.VISIBLE);
        }
        v.setTag(holder);
        return v;
    }

    public static class ViewHolder {
        TextView nom;
        TextView email;
        TextView tel;
        TextView poste;
        ImageView call_btn;
        ImageView email_btn;
    }
    public void showDialog(String name, ArrayList<String> tels){
        android.app.AlertDialog.Builder builderSingle = new android.app.AlertDialog.Builder(context);
        builderSingle.setTitle(name);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, R.layout.item_number_contact);
        arrayAdapter.addAll(tels);

        builderSingle.setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                callNumber(strName);
            }
        });
        builderSingle.show();
    }


    public void callNumber(String tel){
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:"+tel ));
        context.startActivity(intent);
    }

}
