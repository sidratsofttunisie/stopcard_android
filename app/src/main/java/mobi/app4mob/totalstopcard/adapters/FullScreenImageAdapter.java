package mobi.app4mob.totalstopcard.adapters;


import java.util.ArrayList;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import mobi.app4mob.total.stopcard.R;
import mobi.app4mob.totalstopcard.widget.TouchImageView;

public class FullScreenImageAdapter extends PagerAdapter {

    private Activity _activity;
    private LayoutInflater inflater;
    Integer[] imageId;

    // constructor
    public FullScreenImageAdapter(Activity activity) {
        this._activity = activity;
        this.imageId = new Integer[]{
                R.drawable.reg_fr_12,
                R.drawable.reg_fr_1,
                R.drawable.reg_fr_2,
                R.drawable.reg_fr_3,
                R.drawable.reg_fr_4,
                R.drawable.reg_fr_5,
                R.drawable.reg_fr_6,
                R.drawable.reg_fr_7,
                R.drawable.reg_fr_8,
                R.drawable.reg_fr_9,
                R.drawable.reg_fr_10,
                R.drawable.reg_fr_11,
                R.drawable.reg_fr_12,
                R.drawable.reg_fr_1,

        };

        if (Locale.getDefault().getLanguage().equals("en")) {
            this.imageId = new Integer[]{
                    R.drawable.reg_en_12,
                    R.drawable.reg_en_1,
                    R.drawable.reg_en_2,
                    R.drawable.reg_en_3,
                    R.drawable.reg_en_4,
                    R.drawable.reg_en_5,
                    R.drawable.reg_en_6,
                    R.drawable.reg_en_7,
                    R.drawable.reg_en_8,
                    R.drawable.reg_en_9,
                    R.drawable.reg_en_10,
                    R.drawable.reg_en_11,
                    R.drawable.reg_en_12,
                    R.drawable.reg_en_1,

            };
        }
    }

    @Override
    public int getCount() {
        return 14;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        TouchImageView imgDisplay;
        Button btnClose;

        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container,
                false);

        imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.imgDisplay);


        imgDisplay.setImageResource(imageId[position]);

        Bitmap bitmap = BitmapFactory.decodeResource(_activity.getResources(), imageId[position]);
        int pixel = bitmap.getPixel(5, 10);
        viewLayout.setBackgroundColor(Color.rgb(Color.red(pixel), Color.green(pixel), Color.blue(pixel)));
        imgDisplay.setBackgroundColor(Color.rgb(Color.red(pixel), Color.green(pixel), Color.blue(pixel)));


        ((ViewPager) container).addView(viewLayout);

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);

    }

}
