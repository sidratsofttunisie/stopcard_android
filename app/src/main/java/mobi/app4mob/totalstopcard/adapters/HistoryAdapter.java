package mobi.app4mob.totalstopcard.adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import mobi.app4mob.totalstopcard.models.StopCard;
import mobi.app4mob.total.stopcard.R;
import mobi.app4mob.totalstopcard.models.StopCardHistory;

/**
 * Created by adnen on 01/02/2016.
 */
public class HistoryAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;
    Context context;
    ArrayList<StopCard> stopCards;
    int type_stopcard = 1;


    public HistoryAdapter(Context context, ArrayList<StopCard> stopCards,int type) {

        this.context = context;
        this.type_stopcard=type ;
        this.stopCards = stopCards;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getCount() {
        return stopCards.size();
    }

    @Override
    public StopCard getItem(int position) {
        return stopCards.get(position);
    }


    @Override
    public long getItemId(int i) {
        return (long) stopCards.get(i).getId();
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder;
        if (convertView == null) {

            v = inflater.inflate(R.layout.row_history, parent, false);
            holder = new ViewHolder();

            holder.lieu = (TextView) v.findViewById(R.id.zone);
            holder.date = (TextView) v.findViewById(R.id.date);
            holder.badge = (TextView) v.findViewById(R.id.badge);
            holder.cat1 = (ImageView) v.findViewById(R.id.cat1);
            holder.cat2 = (ImageView) v.findViewById(R.id.cat2);
            holder.cat3 = (ImageView) v.findViewById(R.id.cat3);
            holder.cat4 = (ImageView) v.findViewById(R.id.cat4);
            holder.cat5 = (ImageView) v.findViewById(R.id.cat5);
            holder.cat6 = (ImageView) v.findViewById(R.id.cat6);
            holder.cat7 = (ImageView) v.findViewById(R.id.cat7);
            holder.cat8 = (ImageView) v.findViewById(R.id.cat8);
            holder.cat9 = (ImageView) v.findViewById(R.id.cat9);
            holder.cat10 = (ImageView) v.findViewById(R.id.cat10);
            holder.cat11 = (ImageView) v.findViewById(R.id.cat11);
            holder.cat12 = (ImageView) v.findViewById(R.id.cat12);
            holder.photo = (ImageView) v.findViewById(R.id.photo);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        StopCard stopCard = this.getItem(position);
        // holder = new ViewHolder();


        holder.lieu.setText(stopCard.getLieu());
        Picasso.with(context)
                .load(context.getString(R.string.resourses_url) + "uploads/stopcard/" + stopCard.getImage())
                .placeholder(R.drawable.logo) // optional
                .error(R.drawable.logo)
                .fit().centerCrop()
                .into(holder.photo);

        if (type_stopcard==1){
            holder.badge.setText(R.string.stopcard);
            holder.badge.setBackground(context.getResources().getDrawable(R.drawable.stopcard));
        }
        else if(type_stopcard==2){
            holder.badge.setText(R.string._bonpratique);
            holder.badge.setBackground(context.getResources().getDrawable(R.drawable.bonne_pratique));
        }
        else{
            holder.badge.setText(R.string._annomalies);
            holder.badge.setBackground(context.getResources().getDrawable(R.drawable.annomalie));
        }


        if(Locale.getDefault().getDisplayLanguage().equals("English")){
            String date = stopCard.getCreated();
            DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            try {
                Date date1 = formatter.parse(date);
                String newstring = new SimpleDateFormat("MM-dd-yyyy").format(date1);
                holder.date.setText(newstring);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }else

            holder.date.setText(stopCard.getCreated());

        if (stopCard.getType().contains("1"))
            holder.cat1.setVisibility(View.VISIBLE);
        else
            holder.cat1.setVisibility(View.GONE);

        if (stopCard.getType().contains("2"))
            holder.cat2.setVisibility(View.VISIBLE);
        else
            holder.cat2.setVisibility(View.GONE);

        if (stopCard.getType().contains("3"))
            holder.cat3.setVisibility(View.VISIBLE);
        else
            holder.cat3.setVisibility(View.GONE);

        if (stopCard.getType().contains("4"))
            holder.cat4.setVisibility(View.VISIBLE);
        else
            holder.cat4.setVisibility(View.GONE);

        if (stopCard.getType().contains("5"))
            holder.cat5.setVisibility(View.VISIBLE);
        else
            holder.cat5.setVisibility(View.GONE);

        if (stopCard.getType().contains("6"))
            holder.cat6.setVisibility(View.VISIBLE);
        else
            holder.cat6.setVisibility(View.GONE);

        if (stopCard.getType().contains("7"))
            holder.cat7.setVisibility(View.VISIBLE);
        else
            holder.cat7.setVisibility(View.GONE);

        if (stopCard.getType().contains("8"))
            holder.cat8.setVisibility(View.VISIBLE);
        else
            holder.cat8.setVisibility(View.GONE);

        if (stopCard.getType().contains("9"))
            holder.cat9.setVisibility(View.VISIBLE);
        else
            holder.cat9.setVisibility(View.GONE);

        if (stopCard.getType().contains("10"))
            holder.cat10.setVisibility(View.VISIBLE);
        else
            holder.cat10.setVisibility(View.GONE);

        if (stopCard.getType().contains("11"))
            holder.cat11.setVisibility(View.VISIBLE);
        else
            holder.cat11.setVisibility(View.GONE);

        if (stopCard.getType().contains("12"))
            holder.cat12.setVisibility(View.VISIBLE);
        else
            holder.cat12.setVisibility(View.GONE);

        return v;

    }


    public static class ViewHolder {
        TextView lieu;
        TextView badge;
        TextView date;
        ImageView photo;
        ImageView cat1;
        ImageView cat2;
        ImageView cat3;
        ImageView cat4;
        ImageView cat5;
        ImageView cat6;
        ImageView cat7;
        ImageView cat8;
        ImageView cat9;
        ImageView cat10;
        ImageView cat11;
        ImageView cat12;
    }


}
