package mobi.app4mob.totalstopcard.adapters;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import mobi.app4mob.total.stopcard.R;
public class IncidentAdapter extends ArrayAdapter<String>{

    private final Activity context;
    private final String[] web;
    private final Integer[] imageId;
    public Boolean[] selected_incident = new Boolean[12];
    public int[] selected_incident_id = new int[12];
    private Map<String,Boolean> selectes_item =new HashMap<String, Boolean>();

    public IncidentAdapter(Activity context,
                           String[] web, Integer[] imageId, int[] selected_incident_id) {
        super(context, R.layout.row_incident, web);
        this.context = context;
        this.web = web;
        this.imageId = imageId;
        this.selected_incident_id = selected_incident_id;
        selectedItemInitializatio();

    }



    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.row_incident, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
        txtTitle.setText(web[position]);
        CheckBox checkbox = (CheckBox) rowView.findViewById(R.id.checkbox);
        if(selectes_item.get(web[position])){
            checkbox.setChecked(true);
        }else
            checkbox.setChecked(false);

        checkbox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                CheckBox cb = (CheckBox) v;
                View parent = (View) v.getParent();
                TextView text = (TextView) parent.findViewById(R.id.txt);
                cb.setChecked(!selectes_item.get(text.getText().toString()));
                selectes_item.put(text.getText().toString(),cb.isChecked()) ;
                Log.e(text.getText().toString()+"selected", String.valueOf(selectes_item.get(text.getText().toString()))) ;
                selected_incident[position]=cb.isChecked();
            }

        });

        rowView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                CheckBox cb = (CheckBox) v.findViewById(R.id.checkbox);
                cb.setChecked(!cb.isChecked());
                TextView text = (TextView) v.findViewById(R.id.txt);
                selectes_item.put(text.getText().toString(),cb.isChecked()) ;
                selected_incident[position]=cb.isChecked();

            }

        });

        imageView.setImageResource(imageId[position]);
        return rowView;
    }

    public boolean isSelected(int id){
        for (int i=0; i<selected_incident_id.length; i++)
        {
            if(selected_incident_id[i] == id)
                return true;
        }
        return false;
    }
    public int[] getSelectedIncident(){
        int[] result = new int[12];
        int i=0,j = 0;
        Collection<Boolean> values =selectes_item.values();
        Log.i(">>>>>>>>",">>>>>select incidents values<<<<<<"+selected_incident.toString());

        for (Boolean val : selected_incident){
            if(val) {
                result[j] = i+1;
                j++;
            }
            i++;
        }

        return  result;
    }

    private void selectedItemInitializatio() {

        for (int j = 0; j < 12; j++) {
            if (contains(j+1))
                selected_incident[j] = true;
            else
                selected_incident[j] = false;
        }

        for (int i=0 ;i<web.length;i++){
            selectes_item.put(web[i],selected_incident[i]);
        }
    }

    private boolean contains(int i) {
        boolean res = false ;
        for (int c=0;c<selected_incident_id.length;c++){
            if(selected_incident_id[c]==i)
                res=true;

        }
        return res;
    }
}