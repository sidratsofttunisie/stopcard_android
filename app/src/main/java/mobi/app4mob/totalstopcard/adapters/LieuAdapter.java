package mobi.app4mob.totalstopcard.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import mobi.app4mob.totalstopcard.models.Lieu;
import mobi.app4mob.total.stopcard.R;

/**
 * Created by adnen on 01/02/2016.
 */
public class LieuAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;
    Context context;
    ArrayList<Lieu> lieux;

    public LieuAdapter(Context context, ArrayList<Lieu> lieux) {

        this.context = context;
        this.lieux = lieux;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }


    @Override
    public int getCount() {
        return lieux.size();
    }

    @Override
    public Lieu getItem(int position) {
        return lieux.get(position);
    }


    @Override
    public long getItemId(int i) {
        return (long) lieux.get(i).getId();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder;
        if (convertView == null) {
            v = inflater.inflate(R.layout.row_lieu, parent, false);
            holder = new ViewHolder();
        } else {
            holder = (ViewHolder) v.getTag();
        }
            Lieu lieu = this.getItem(position);

            holder.lieu = (TextView) v.findViewById(R.id.lieu);

            holder.lieu.setText(lieu.getLibelle());
            v.setTag(holder);


        return v;

    }


    public static class ViewHolder {
        TextView lieu;
    }


}
