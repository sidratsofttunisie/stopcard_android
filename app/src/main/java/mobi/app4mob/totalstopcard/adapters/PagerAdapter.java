package mobi.app4mob.totalstopcard.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.google.gson.Gson;

import java.util.ArrayList;

import mobi.app4mob.totalstopcard.Fragments.AnnomalieFragment;
import mobi.app4mob.totalstopcard.Fragments.BonnePratiqueFragment;
import mobi.app4mob.totalstopcard.Fragments.StopCardFragment;
import mobi.app4mob.totalstopcard.models.StopCardHistory;


/**
 * Created by caisse on 09/02/2018.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    StopCardHistory history ;

    public PagerAdapter(FragmentManager fm, int NumOfTabs, StopCardHistory data) {
        super(fm);
        this.mNumOfTabs = NumOfTabs ;
        this.history = data ;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Bundle bundle = new Bundle();
                bundle.putSerializable("list", history.getAnnomalies());
                AnnomalieFragment tab1 = new AnnomalieFragment();
                tab1.setArguments(bundle);
                return tab1;
            case 1:
                Bundle bundle1 = new Bundle();
                bundle1.putSerializable("list", history.getStopcards());
                StopCardFragment tab2 = new StopCardFragment();
                tab2.setArguments(bundle1);
                return tab2;
            case 2:
                Bundle bundle2 = new Bundle();
                bundle2.putSerializable("list", history.getBonnes_pratiques());
                BonnePratiqueFragment tab3 = new BonnePratiqueFragment();
                tab3.setArguments(bundle2);
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return this.mNumOfTabs;
    }
}
