package mobi.app4mob.totalstopcard.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import mobi.app4mob.total.stopcard.R;
import mobi.app4mob.totalstopcard.models.SafetyNotification;
import mobi.app4mob.totalstopcard.widget.NavDrawerItem;


public class SafetyCheckListAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<SafetyNotification> notifications;
	private static LayoutInflater inflater = null;

	public SafetyCheckListAdapter(Context context, ArrayList<SafetyNotification> notifs){
		this.context = context;
		this.notifications = notifs;
		this.inflater = (LayoutInflater)
				context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return notifications.size();
	}

	@Override
	public Object getItem(int position) {
		return notifications.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		SafetyCheckListAdapter.ViewHolder holder;
		if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_safety_cehck, null);
			holder = new ViewHolder();

			holder.date = (TextView) convertView.findViewById(R.id.date);
			holder.status = (ImageView) convertView.findViewById(R.id.status);
			holder.corp = (TextView) convertView.findViewById(R.id.corp);

			convertView.setTag(holder);
        }else
			holder = (SafetyCheckListAdapter.ViewHolder) convertView.getTag();

		SafetyNotification notification =notifications.get(position);

		holder.date.setText(notification.getDate());
		holder.corp.setText(notification.getNotif());
		if(notification.getReponse()==1)
			holder.status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_v));
		else if(notification.getReponse()==2)
			holder.status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_r));
		else if(notification.getReponse()==3)
			holder.status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_g));
		else
			holder.status.setVisibility(View.GONE);
        return convertView;
	}
	public static class ViewHolder {

		TextView date;
		ImageView status;
		TextView corp;

	}
}
