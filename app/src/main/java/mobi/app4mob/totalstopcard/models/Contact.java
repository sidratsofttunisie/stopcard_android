package mobi.app4mob.totalstopcard.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by adnen.chouibi@gmail.com on 24/02/2017.
 */
public class Contact implements Serializable {

    @SerializedName("nom")
    private String nom;
    @SerializedName("poste")
    private String poste;
    @SerializedName("tel")
    private ArrayList<String> tel = new ArrayList<String>();
    @SerializedName("email")
    private String email;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPoste() {
        return poste;
    }

    public void setPoste(String poste) {
        this.poste = poste;
    }

    public ArrayList<String> getTel() {
        return tel;
    }

    public void setTel(ArrayList<String> tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

}
