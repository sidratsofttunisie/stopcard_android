package mobi.app4mob.totalstopcard.models;

/**
 * Created by adnen.chouibi@gmail.com on 27/02/2017.
 */

public class GoldenRules {

    private String image;
    private String titre;
    private String description;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
