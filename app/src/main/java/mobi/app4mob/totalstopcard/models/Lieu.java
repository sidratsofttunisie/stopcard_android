package mobi.app4mob.totalstopcard.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by salma on 19/04/2016.
 */
public class Lieu implements Serializable {

    @SerializedName("id")
    private int id;
    @SerializedName("libelle")
    private String libelle;

    /**
     *
     * @return
     * The id
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     *
     * @param libelle
     * The libelle
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
