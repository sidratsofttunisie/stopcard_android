package mobi.app4mob.totalstopcard.models;

/**
 * Created by root on 18/04/2016.
 */

import com.google.gson.annotations.SerializedName;


/**
 * Created by adnen on 26/01/2016.
 */
public class PostResponse {


    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private String data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "message= "+this.message+" === date = "+this.data;
    }
}
