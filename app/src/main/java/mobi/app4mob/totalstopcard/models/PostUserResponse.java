package mobi.app4mob.totalstopcard.models;

import com.google.gson.annotations.SerializedName;


/**
 * Created by adnen on 26/01/2016.
 */
public class PostUserResponse {


    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private User data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getData() {
        return data;
    }

    public void setData(User data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "message= "+this.message+" === date = "+this.data;
    }
}
