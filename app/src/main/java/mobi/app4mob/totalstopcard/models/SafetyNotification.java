package mobi.app4mob.totalstopcard.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by root on 14/04/2016.
 */
public class SafetyNotification implements Serializable {


    @SerializedName("id")
    private Integer id;

    @SerializedName("reponse")
    private int reponse;

    @SerializedName("date")
    private String date;

    @SerializedName("notif")
    private String notif;

    @SerializedName("comment")
    private String comment;



    public SafetyNotification(Integer id, String notif, String date, int reponse,String comment) {
        this.id = id;
        this.notif = notif;
        this.date = date;
        this.reponse = reponse;
        this.comment = comment ;

    }

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    public int getReponse() {
        return reponse;
    }

    public void setReponse(int reponse) {
        this.reponse = reponse;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNotif() {
        return notif;
    }

    public void setNotif(String notif) {
        this.notif = notif;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
