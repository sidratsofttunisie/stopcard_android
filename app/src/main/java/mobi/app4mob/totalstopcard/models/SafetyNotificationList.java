package mobi.app4mob.totalstopcard.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by caisse on 06/02/2018.
 */

public class SafetyNotificationList {
    @SerializedName("data")
    private ArrayList<SafetyNotification> safetyNotifications;

    @SerializedName("message")
    private String message;


    public ArrayList<SafetyNotification> getSafetyNotifications() {
        return safetyNotifications;
    }

    public void setSafetyNotifications(ArrayList<SafetyNotification> safetyNotifications) {
        this.safetyNotifications = safetyNotifications;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
