package mobi.app4mob.totalstopcard.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by caisse on 16/02/2018.
 */

public class ServerResponse {
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private Data data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    class Data {
        int id ;
    }
}
