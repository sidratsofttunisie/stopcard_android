package mobi.app4mob.totalstopcard.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by root on 16/04/2016.
 */
public class StopCard implements Serializable{
    @SerializedName("id")
    private Integer id;
    @SerializedName("lieu")
    private String lieu;
    @SerializedName("created")
    private String created;
    @SerializedName("image")
    private String image;
    @SerializedName("description_stopcard")
    private String descriptionStopcard;
    @SerializedName("comment_hse")
    private String comment_hse;
    @SerializedName("type")
    private ArrayList<String> type = new ArrayList<String>();
    @SerializedName("action")
    private String action;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The lieu
     */
    public String getLieu() {
        return lieu;
    }

    /**
     *
     * @param lieu
     * The lieu
     */
    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    /**
     *
     * @return
     * The created
     */
    public String getCreated() {
        return created;
    }

    /**
     *
     * @param created
     * The created
     */
    public void setCreated(String created) {
        this.created = created;
    }

    /**
     *
     * @return
     * The image
     */
    public String getImage() {
        return image;
    }

    /**
     *
     * @param image
     * The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     *
     * @return
     * The descriptionStopcard
     */
    public String getDescriptionStopcard() {
        return descriptionStopcard;
    }

    /**
     *
     * @param descriptionStopcard
     * The description_stopcard
     */
    public void setDescriptionStopcard(String descriptionStopcard) {
        this.descriptionStopcard = descriptionStopcard;
    }

    /**
     *
     * @return
     * The type
     */
    public ArrayList<String> getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(ArrayList<String> type) {
        this.type = type;
    }

    public String getComment_hse() {
        return comment_hse;
    }

    public void setComment_hse(String comment_hse) {
        this.comment_hse = comment_hse;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
