package mobi.app4mob.totalstopcard.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by caisse on 06/02/2018.
 */

public class StopCardHistory {
    @SerializedName("stopcards")
    private ArrayList<StopCard> stopcards;

    @SerializedName("annomalies")
    private ArrayList<StopCard> annomalies;

    @SerializedName("bonnes_pratiques")
    private ArrayList<StopCard> bonnes_pratiques;

    public ArrayList<StopCard> getStopcards() {
        return stopcards;
    }

    public void setStopcards(ArrayList<StopCard> stopcards) {
        this.stopcards = stopcards;
    }

    public ArrayList<StopCard> getAnnomalies() {
        return annomalies;
    }

    public void setAnnomalies(ArrayList<StopCard> annomalies) {
        this.annomalies = annomalies;
    }

    public ArrayList<StopCard> getBonnes_pratiques() {
        return bonnes_pratiques;
    }

    public void setBonnes_pratiques(ArrayList<StopCard> bonnes_pratiques) {
        this.bonnes_pratiques = bonnes_pratiques;
    }
}
