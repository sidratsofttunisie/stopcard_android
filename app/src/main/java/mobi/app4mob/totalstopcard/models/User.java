package mobi.app4mob.totalstopcard.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 14/04/2016.
 */
public class User {


    @SerializedName("id")
    private Integer id;

    @SerializedName("username")
    private String username;

    @SerializedName("nom")
    private String nom;

    @SerializedName("prenom")
    private String prenom;

    @SerializedName("country")
    private String country;

    @SerializedName("nb_stop_cards")
    private Integer nbStopCards=0;

    @SerializedName("nb_annomalies")
    private Integer nbAnnomalies=0;

    @SerializedName("nb_bonnes_pratiques")
    private Integer nbBonPratiques=0;

    @SerializedName("can_see_all")
    private Boolean can_see_all;

    @SerializedName("has_notif")
    private Boolean has_notif;

    @SerializedName("has_annomalies_bonnes_pratiques")
    private Boolean has_annomalies_bonnes_pratiques;

    @SerializedName("has_annomalies")
    private Boolean has_annomalies;

    @SerializedName("has_bonnes_pratiques")
    private Boolean has_bonnes_pratiques;

    @SerializedName("has_stop_card")
    private Boolean has_stop_card;



    public User(Integer id, String username, String nom, String prenom, String country, Integer nbStopCards, Integer nbAnnomalies,
                Integer nbBonPratiques, Boolean can_see_all, Boolean has_notif,
                Boolean has_annomalies_bonnes_pratiques,Boolean has_annomalies,Boolean has_bonnes_pratiques,
                Boolean has_stop_card) {
        this.id = id;
        this.username = username;
        this.nom = nom;
        this.prenom = prenom;
        this.country = country;
        this.nbStopCards = nbStopCards;
        this.can_see_all = can_see_all;
        this.nbAnnomalies = nbAnnomalies;
        this.nbBonPratiques = nbBonPratiques ;
        this.has_annomalies_bonnes_pratiques = has_annomalies_bonnes_pratiques;
        this.has_annomalies = has_annomalies;
        this.has_bonnes_pratiques = has_bonnes_pratiques;
        this.has_stop_card = has_stop_card;
        this.has_notif = has_notif;
    }

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return The nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom The nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return The prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param prenom The prenom
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * @return The country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country The country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return The nbStopCards
     */
    public Integer getNbStopCards() {
        return nbStopCards;
    }

    /**
     * @param nbStopCards The nb_stop_cards
     */
    public void setNbStopCards(Integer nbStopCards) {
       this.nbStopCards = nbStopCards;
    }
    public void setNbBP(Integer nbStopCards) {
        this.nbBonPratiques = nbStopCards;

    }
    public void setNbAnolay(Integer nbStopCards) {
        this.nbAnnomalies = nbStopCards;
    }


    public Boolean getCan_see_all() {
        return can_see_all;
    }

    public void setCan_see_all(Boolean can_see_all) {
        this.can_see_all = can_see_all;
    }

    public Integer getNbAnnomalies() {
        return nbAnnomalies;
    }

    public void setNbAnnomalies(Integer nbAnnomalies) {
        this.nbAnnomalies = nbAnnomalies;
    }

    public Integer getNbBonPratiques() {
        return nbBonPratiques;
    }

    public void setNbBonPratiques(Integer nbBonPratiques) {
        this.nbBonPratiques = nbBonPratiques;
    }

    public Boolean getHas_notif() {
        return has_notif;
    }

    public void setHas_notif(Boolean has_notif) {
        this.has_notif = has_notif;
    }

    public Boolean getHas_annomalies_bonnes_pratiques() {
        return has_annomalies_bonnes_pratiques;
    }

    public void setHas_annomalies_bonnes_pratiques(Boolean has_annomalies_bonnes_pratiques) {
        this.has_annomalies_bonnes_pratiques = has_annomalies_bonnes_pratiques;
    }

    public Boolean getHas_annomalies() {
        return has_annomalies;
    }

    public void setHas_annomalies(Boolean has_annomalies) {
        this.has_annomalies = has_annomalies;
    }

    public Boolean getHas_bonnes_pratiques() {
        return has_bonnes_pratiques;
    }

    public void setHas_bonnes_pratiques(Boolean has_bonnes_pratiques) {
        this.has_bonnes_pratiques = has_bonnes_pratiques;
    }

    public Boolean getHas_stop_card() {
        return has_stop_card;
    }

    public void setHas_stop_card(Boolean has_stop_card) {
        this.has_stop_card = has_stop_card;
    }
}
