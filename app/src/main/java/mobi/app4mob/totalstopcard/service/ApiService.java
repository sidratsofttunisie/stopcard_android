package mobi.app4mob.totalstopcard.service;


import com.squareup.okhttp.RequestBody;

import java.util.ArrayList;

import mobi.app4mob.totalstopcard.models.Contact;
import mobi.app4mob.totalstopcard.models.Lieu;
import mobi.app4mob.totalstopcard.models.PostResponse;
import mobi.app4mob.totalstopcard.models.PostUserResponse;
import mobi.app4mob.totalstopcard.models.SafetyNotification;
import mobi.app4mob.totalstopcard.models.SafetyNotificationList;
import mobi.app4mob.totalstopcard.models.ServerResponse;
import mobi.app4mob.totalstopcard.models.StopCard;
import mobi.app4mob.totalstopcard.models.StopCardHistory;
import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;

/**
 * Created by adnen on 18/01/2016.
 */
public interface ApiService {
    public static String SUCCESS = "success";
    public static String ERROR = "error";
    public static String EXPIRED = "fail";


    @FormUrlEncoded
    @POST("login")
    Call<PostUserResponse> login(@Field("username") String username, @Field("password") String password, @Field("android_token") String android_token );


    @GET("get_user_information_v2")
    Call<PostUserResponse> getUserInformation(@Query("user_id") int personel_id);

    @FormUrlEncoded
    @POST("account/edit")
    Call<PostResponse> editUser(@Field("username") String username, @Field("password") String password, @Field("nom") String nom, @Field("prenom") String prenom,  @Field("id") String id );


    @FormUrlEncoded
    @POST("forgotpassword")
    Call<PostResponse> forgotpassword(@Field("username") String username ,@Field("locale") String locale);


    @GET("history_v2")
    Call<StopCardHistory> getHistory(@Query("personnel_id") int personel_id);

    @GET("notifications_list")
    Call<SafetyNotificationList> getSafetyNotification(@Query("user_id") int personel_id);
    @GET("notifications")
    Call<SafetyNotification> getNotification(@Query("user_id") int personel_id,@Query("notif_id") int notif_id);

    @FormUrlEncoded
    @POST("post_notification")
    Call<ServerResponse> postSafetyNotification(
            @Field("user_id") int personel_id,
            @Field("notification_id") int notification_id,
            @Field("response") int response,
            @Field("latitude") double latitude,
            @Field("longitude") double longitude,
            @Field("comment") String comment
    );

    @GET("lastStopcard_v2")
    Call<StopCardHistory> getLastStopCard(@Query("pays") String pays);

    @GET("getUser_v2")
    Call<PostUserResponse> getUser(@Query("id") int id, @Query("android_token") String android_token);

    @GET("lieu")
    Call<ArrayList<Lieu>> getLieux(@Query("pays") String pays,@Query("latitude")  double latitude, @Query("longitude")  double longitude);

    @GET("contact")
    Call<ArrayList<Contact>> getContacts(@Query("pays") String pays);


    @Multipart
    @POST("addStopCard")
    Call<PostResponse> addPublication(
            @Part("myfile\"; filename=\"image.png\" ") RequestBody file,
            @Part("description_stopcard") RequestBody description,
            @Part("action_stopcard") RequestBody action,
            @Part("categories") RequestBody categories,
            @Part("type") int type,
            @Part("user_id") RequestBody user_id,
            @Part("lieu_id") RequestBody lieu_id,
            @Part("locale") RequestBody locale

            );

    @FormUrlEncoded
    @POST("addStopCard")
    Call<PostResponse> addPublicationWithoutImage( @Field("description_stopcard") String description,
                                                   @Field("action_stopcard") String action,
                                                   @Field("categories") String categories,
                                                   @Field("user_id") String user_id,
                                                   @Field("type") int type,
                                                   @Field("lieu_id") String lieu_id,
                                                   @Field("locale") String locale);

}
