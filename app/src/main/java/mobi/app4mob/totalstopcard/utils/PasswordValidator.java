package mobi.app4mob.totalstopcard.utils;

import android.text.TextUtils;
import android.widget.EditText;

import com.andreabaccega.formedittextvalidator.Validator;
/**
 * Created by salma on 25/04/2016.
 */
public class PasswordValidator extends Validator {

    public PasswordValidator() {
        super("You should enter 'ciao' here");
    }

    public boolean isValid(EditText et) {
        return TextUtils.equals(et.getText(), "ciao");
    }

}