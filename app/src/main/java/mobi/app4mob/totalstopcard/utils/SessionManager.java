package mobi.app4mob.totalstopcard.utils;

/**
 * Created by root on 16/04/2016.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;

import mobi.app4mob.totalstopcard.models.User;


/**
 * Created by adnen on 21/01/2016.
 */

public class SessionManager {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "StopcardPref";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    private static final String WITH_LONG_SESSION = "WithLongSession";

    public static final String ID = "user_id";

    public static final String USERNAME = "user_name";

    public static final String NOM = "nom";

    public static final String PRENOM = "prenom";

    public static final String COUNTRY = "country";

    public static final String NBSTOPCARD = "nb_stop_cards";

    public static final String NBANNOMALIES = "nb_annomalies";

    public static final String NBBONNEPRATIQUES = "nb_bonne_pratiques";

    public static final String CANSEEALL = "can_see_all";

    public static final String HASNOTIF = "has_notif";

    public static final String HASANNOMALIESBONNEPRATIQUES = "has_annomalies_bonne_pratiques";

    public static final String HASBONNEPRATIQUES = "has_bonne_pratiques";

    public static final String HASANNOMALIES = "has_annomalies";

    public static final String HASSTOPCARD = "has_stop_card";

    public static final String REG_TOKEN = "reg_token";


    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     *
     * @param user
     */
    public void createLoginSession(User user, Boolean with_long_session) {
        Gson gson = new Gson();
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        editor.putBoolean(WITH_LONG_SESSION, with_long_session);

        // Storing data in pref
        editor.putInt(ID, user.getId());
        editor.putString(USERNAME, user.getUsername());
        editor.putString(NOM, user.getNom());
        editor.putString(PRENOM, user.getPrenom());
        editor.putString(COUNTRY, user.getCountry());
        editor.putInt(NBSTOPCARD, user.getNbStopCards());
        editor.putBoolean(CANSEEALL, user.getCan_see_all());
        editor.putBoolean(HASNOTIF, user.getHas_notif());
        editor.putBoolean(HASANNOMALIESBONNEPRATIQUES, user.getHas_annomalies_bonnes_pratiques());
        editor.putBoolean(HASANNOMALIES, user.getHas_annomalies());
        editor.putBoolean(HASBONNEPRATIQUES, user.getHas_bonnes_pratiques());
        editor.putBoolean(HASSTOPCARD, user.getHas_stop_card());

        if(user.getHas_annomalies()) {
            editor.putInt(NBANNOMALIES, user.getNbAnnomalies());
        }
        if(user.getHas_bonnes_pratiques()) {
            editor.putInt(NBBONNEPRATIQUES, user.getNbBonPratiques());
        }



        // commit changes
        editor.commit();
        // user information login in fabric crashlytics
        saveloggedUserInformationInFabric();
    }

    /**
     * Update login session
     *
     * @param user
     */
    public void updateSession(User user) {

        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        Log.e("update sess",String.valueOf(user.getHas_notif()));
        // Storing data in pref
        editor.putInt(ID, user.getId());
        editor.putString(USERNAME, user.getUsername());
        editor.putString(NOM, user.getNom());
        editor.putString(PRENOM, user.getPrenom());
        editor.putString(COUNTRY, user.getCountry());
        editor.putInt(NBSTOPCARD, user.getNbStopCards());
        editor.putBoolean(CANSEEALL, user.getCan_see_all());
        editor.putBoolean(HASNOTIF, user.getHas_notif());
        editor.putBoolean(HASANNOMALIESBONNEPRATIQUES, user.getHas_annomalies_bonnes_pratiques());

        editor.putBoolean(HASANNOMALIES, user.getHas_annomalies());
        editor.putBoolean(HASBONNEPRATIQUES, user.getHas_bonnes_pratiques());
        editor.putBoolean(HASSTOPCARD, user.getHas_stop_card());

        if(user.getHas_annomalies()){
            editor.putInt(NBANNOMALIES, user.getNbAnnomalies());
        }
        if(user.getHas_bonnes_pratiques()){
            editor.putInt(NBBONNEPRATIQUES, user.getNbBonPratiques());
        }
        // commit changes
        editor.commit();
        // user information login in fabric crashlytics
        saveloggedUserInformationInFabric();

    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     */
    public Boolean checkLogin() {
        // Check login status
        return this.isLoggedIn();
    }
    private void saveloggedUserInformationInFabric() {
        try{
            Crashlytics.setUserIdentifier(pref.getInt(ID, -1)+"");
            Crashlytics.setUserName(pref.getString(USERNAME, ""));
            Crashlytics.setInt("user ID",pref.getInt(ID, -1));
            Crashlytics.setString("user name",pref.getString(USERNAME, ""));
            Crashlytics.setString("country",pref.getString(COUNTRY, ""));
            Crashlytics.setBool("can see all",pref.getBoolean(CANSEEALL, true));
            Crashlytics.setBool("has notif",pref.getBoolean(HASNOTIF, false));
            Crashlytics.setBool("has anomalie bonne pratique",pref.getBoolean(HASANNOMALIESBONNEPRATIQUES, false));
            Crashlytics.setBool("has anomalie",pref.getBoolean(HASANNOMALIES, false));
            Crashlytics.setBool("has bonne pratique",pref.getBoolean(HASBONNEPRATIQUES, false));

        }catch (Exception e){
            Crashlytics.logException(e);
        }

    }


    /**
     * Get stored session data
     */
    public User getUserDetails() {

        Gson gson = new Gson();
        User user;

        //intit user
        user = new User(
                pref.getInt(ID, -1),
                pref.getString(USERNAME, ""),
                pref.getString(NOM, ""),
                pref.getString(PRENOM, ""),
                pref.getString(COUNTRY, ""),
                pref.getInt(NBSTOPCARD, 0),
                pref.getInt(NBANNOMALIES, 0),
                pref.getInt(NBBONNEPRATIQUES, 0),
                pref.getBoolean(CANSEEALL, true),
                pref.getBoolean(HASNOTIF, false),
                pref.getBoolean(HASANNOMALIESBONNEPRATIQUES, false),
                pref.getBoolean(HASANNOMALIES, false),
                pref.getBoolean(HASBONNEPRATIQUES, false),
                pref.getBoolean(HASSTOPCARD, false)



        );

        // return user
        return user;
    }

    /**
     * Clear session details
     */
    public void logoutUser() {

        int id=pref.getInt(ID, -1);
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.putInt(ID, id);
        editor.commit();

    }

    /**
     * Quick check for login
     * *
     */
    // Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    /**
     * Quick check garder session
     * *
     */
    // Get Session State
    public boolean withLongSession() {
        return pref.getBoolean(WITH_LONG_SESSION, false);
    }

    public String getRegToken() {
        return pref.getString(REG_TOKEN, "");
    }

    public void setRegToken(String reg_token) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing data in pref
        editor.putString(REG_TOKEN, reg_token);
        editor.commit();
    }
}
