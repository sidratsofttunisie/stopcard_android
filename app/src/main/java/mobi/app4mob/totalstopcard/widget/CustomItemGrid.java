package mobi.app4mob.totalstopcard.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by adnen.chouibi@gmail.com on 13/03/2017.
 */

public class CustomItemGrid extends LinearLayout {
    public CustomItemGrid(Context context) {
        super(context);
    }

    public CustomItemGrid(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomItemGrid(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
